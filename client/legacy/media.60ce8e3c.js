import { c as _inherits, d as _getPrototypeOf, e as _possibleConstructorReturn, f as _classCallCheck, i as init, s as safe_not_equal, g as _assertThisInitialized, h as dispatch_dev, j as _createClass, S as SvelteComponentDev, a4 as validate_each_argument, v as validate_slots, K as Icon, ac as empty, z as insert_dev, R as transition_in, a8 as group_outros, T as transition_out, a9 as check_outros, q as detach_dev, l as element, n as claim_element, p as children, u as attr_dev, x as add_location, b as _slicedToArray, aa as destroy_each, m as space, L as text, M as create_component, t as claim_space, N as claim_text, O as claim_component, y as set_style, A as append_dev, P as mount_component, Q as set_data_dev, U as destroy_component, G as noop, a as _asyncToGenerator, r as regenerator, ad as query_selector_all } from './client.dc955f77.js';
import { _ as _defineProperty } from './defineProperty.bdf5641a.js';
import { C as Container } from './Container.19136de0.js';
import { B as Button, P as PageCTA } from './PageCTA.ddb524e9.js';

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
var file = "src/components/ArticleList.svelte";

function get_each_context(ctx, list, i) {
  var child_ctx = ctx.slice();
  child_ctx[2] = list[i].url;
  child_ctx[3] = list[i].image;
  child_ctx[4] = list[i].date;
  child_ctx[5] = list[i].title;
  child_ctx[6] = list[i].body;
  return child_ctx;
} // (20:4) {#if title}


function create_if_block(ctx) {
  var li;
  var article;
  var t0;
  var t1;
  var h3;
  var t2_value =
  /*title*/
  ctx[5] + "";
  var t2;
  var t3;
  var t4;
  var p;
  var button;
  var t5;
  var current;
  var if_block0 =
  /*image*/
  ctx[3] && create_if_block_3(ctx);
  var if_block1 =
  /*date*/
  ctx[4] && create_if_block_2(ctx);
  var if_block2 =
  /*body*/
  ctx[6] && create_if_block_1(ctx);
  button = new Button({
    props: {
      href:
      /*url*/
      ctx[2],
      target: "_blank",
      icon: "ARROW_EXTERNAL",
      $$slots: {
        default: [create_default_slot]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      li = element("li");
      article = element("article");
      if (if_block0) if_block0.c();
      t0 = space();
      if (if_block1) if_block1.c();
      t1 = space();
      h3 = element("h3");
      t2 = text(t2_value);
      t3 = space();
      if (if_block2) if_block2.c();
      t4 = space();
      p = element("p");
      create_component(button.$$.fragment);
      t5 = space();
      this.h();
    },
    l: function claim(nodes) {
      li = claim_element(nodes, "LI", {
        class: true
      });
      var li_nodes = children(li);
      article = claim_element(li_nodes, "ARTICLE", {
        class: true
      });
      var article_nodes = children(article);
      if (if_block0) if_block0.l(article_nodes);
      t0 = claim_space(article_nodes);
      if (if_block1) if_block1.l(article_nodes);
      t1 = claim_space(article_nodes);
      h3 = claim_element(article_nodes, "H3", {});
      var h3_nodes = children(h3);
      t2 = claim_text(h3_nodes, t2_value);
      h3_nodes.forEach(detach_dev);
      t3 = claim_space(article_nodes);
      if (if_block2) if_block2.l(article_nodes);
      t4 = claim_space(article_nodes);
      p = claim_element(article_nodes, "P", {
        style: true
      });
      var p_nodes = children(p);
      claim_component(button.$$.fragment, p_nodes);
      p_nodes.forEach(detach_dev);
      article_nodes.forEach(detach_dev);
      t5 = claim_space(li_nodes);
      li_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      add_location(h3, file, 32, 10, 735);
      set_style(p, "text-align", "right");
      add_location(p, file, 37, 10, 841);
      attr_dev(article, "class", "svelte-jpws6w");
      add_location(article, file, 21, 6, 483);
      attr_dev(li, "class", "svelte-jpws6w");
      add_location(li, file, 20, 4, 472);
    },
    m: function mount(target, anchor) {
      insert_dev(target, li, anchor);
      append_dev(li, article);
      if (if_block0) if_block0.m(article, null);
      append_dev(article, t0);
      if (if_block1) if_block1.m(article, null);
      append_dev(article, t1);
      append_dev(article, h3);
      append_dev(h3, t2);
      append_dev(article, t3);
      if (if_block2) if_block2.m(article, null);
      append_dev(article, t4);
      append_dev(article, p);
      mount_component(button, p, null);
      append_dev(li, t5);
      current = true;
    },
    p: function update(ctx, dirty) {
      if (
      /*image*/
      ctx[3]) {
        if (if_block0) {
          if_block0.p(ctx, dirty);
        } else {
          if_block0 = create_if_block_3(ctx);
          if_block0.c();
          if_block0.m(article, t0);
        }
      } else if (if_block0) {
        if_block0.d(1);
        if_block0 = null;
      }

      if (
      /*date*/
      ctx[4]) {
        if (if_block1) {
          if_block1.p(ctx, dirty);
        } else {
          if_block1 = create_if_block_2(ctx);
          if_block1.c();
          if_block1.m(article, t1);
        }
      } else if (if_block1) {
        if_block1.d(1);
        if_block1 = null;
      }

      if ((!current || dirty &
      /*articles*/
      1) && t2_value !== (t2_value =
      /*title*/
      ctx[5] + "")) set_data_dev(t2, t2_value);

      if (
      /*body*/
      ctx[6]) {
        if (if_block2) {
          if_block2.p(ctx, dirty);
        } else {
          if_block2 = create_if_block_1(ctx);
          if_block2.c();
          if_block2.m(article, t4);
        }
      } else if (if_block2) {
        if_block2.d(1);
        if_block2 = null;
      }

      var button_changes = {};
      if (dirty &
      /*articles*/
      1) button_changes.href =
      /*url*/
      ctx[2];

      if (dirty &
      /*$$scope*/
      512) {
        button_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      button.$set(button_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(button.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(button.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(li);
      if (if_block0) if_block0.d();
      if (if_block1) if_block1.d();
      if (if_block2) if_block2.d();
      destroy_component(button);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block.name,
    type: "if",
    source: "(20:4) {#if title}",
    ctx: ctx
  });
  return block;
} // (23:8) {#if image}


function create_if_block_3(ctx) {
  var picture;
  var img;
  var img_src_value;
  var img_alt_value;
  var block = {
    c: function create() {
      picture = element("picture");
      img = element("img");
      this.h();
    },
    l: function claim(nodes) {
      picture = claim_element(nodes, "PICTURE", {
        class: true
      });
      var picture_nodes = children(picture);
      img = claim_element(picture_nodes, "IMG", {
        src: true,
        alt: true,
        class: true
      });
      picture_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      if (img.src !== (img_src_value =
      /*image*/
      ctx[3])) attr_dev(img, "src", img_src_value);
      attr_dev(img, "alt", img_alt_value =
      /*title*/
      ctx[5]);
      attr_dev(img, "class", "svelte-jpws6w");
      add_location(img, file, 24, 12, 545);
      attr_dev(picture, "class", "svelte-jpws6w");
      add_location(picture, file, 23, 10, 523);
    },
    m: function mount(target, anchor) {
      insert_dev(target, picture, anchor);
      append_dev(picture, img);
    },
    p: function update(ctx, dirty) {
      if (dirty &
      /*articles*/
      1 && img.src !== (img_src_value =
      /*image*/
      ctx[3])) {
        attr_dev(img, "src", img_src_value);
      }

      if (dirty &
      /*articles*/
      1 && img_alt_value !== (img_alt_value =
      /*title*/
      ctx[5])) {
        attr_dev(img, "alt", img_alt_value);
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(picture);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_3.name,
    type: "if",
    source: "(23:8) {#if image}",
    ctx: ctx
  });
  return block;
} // (29:10) {#if date}


function create_if_block_2(ctx) {
  var time;
  var t_value =
  /*date*/
  ctx[4].split(" ")[0].split("T")[0] + "";
  var t;
  var block = {
    c: function create() {
      time = element("time");
      t = text(t_value);
      this.h();
    },
    l: function claim(nodes) {
      time = claim_element(nodes, "TIME", {
        class: true
      });
      var time_nodes = children(time);
      t = claim_text(time_nodes, t_value);
      time_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(time, "class", "small");
      add_location(time, file, 29, 12, 646);
    },
    m: function mount(target, anchor) {
      insert_dev(target, time, anchor);
      append_dev(time, t);
    },
    p: function update(ctx, dirty) {
      if (dirty &
      /*articles*/
      1 && t_value !== (t_value =
      /*date*/
      ctx[4].split(" ")[0].split("T")[0] + "")) set_data_dev(t, t_value);
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(time);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_2.name,
    type: "if",
    source: "(29:10) {#if date}",
    ctx: ctx
  });
  return block;
} // (35:10) {#if body}


function create_if_block_1(ctx) {
  var p;
  var t_value =
  /*ellipsis*/
  ctx[1](
  /*body*/
  ctx[6], 200) + "";
  var t;
  var block = {
    c: function create() {
      p = element("p");
      t = text(t_value);
      this.h();
    },
    l: function claim(nodes) {
      p = claim_element(nodes, "P", {});
      var p_nodes = children(p);
      t = claim_text(p_nodes, t_value);
      p_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      add_location(p, file, 35, 12, 786);
    },
    m: function mount(target, anchor) {
      insert_dev(target, p, anchor);
      append_dev(p, t);
    },
    p: function update(ctx, dirty) {
      if (dirty &
      /*articles*/
      1 && t_value !== (t_value =
      /*ellipsis*/
      ctx[1](
      /*body*/
      ctx[6], 200) + "")) set_data_dev(t, t_value);
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(p);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_1.name,
    type: "if",
    source: "(35:10) {#if body}",
    ctx: ctx
  });
  return block;
} // (39:12) <Button href={url} target="_blank" icon="ARROW_EXTERNAL">


function create_default_slot(ctx) {
  var t;
  var block = {
    c: function create() {
      t = text("Read more");
    },
    l: function claim(nodes) {
      t = claim_text(nodes, "Read more");
    },
    m: function mount(target, anchor) {
      insert_dev(target, t, anchor);
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(t);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot.name,
    type: "slot",
    source: "(39:12) <Button href={url} target=\\\"_blank\\\" icon=\\\"ARROW_EXTERNAL\\\">",
    ctx: ctx
  });
  return block;
} // (19:2) {#each articles as { url, image, date, title, body }}


function create_each_block(ctx) {
  var if_block_anchor;
  var current;
  var if_block =
  /*title*/
  ctx[5] && create_if_block(ctx);
  var block = {
    c: function create() {
      if (if_block) if_block.c();
      if_block_anchor = empty();
    },
    l: function claim(nodes) {
      if (if_block) if_block.l(nodes);
      if_block_anchor = empty();
    },
    m: function mount(target, anchor) {
      if (if_block) if_block.m(target, anchor);
      insert_dev(target, if_block_anchor, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      if (
      /*title*/
      ctx[5]) {
        if (if_block) {
          if_block.p(ctx, dirty);

          if (dirty &
          /*articles*/
          1) {
            transition_in(if_block, 1);
          }
        } else {
          if_block = create_if_block(ctx);
          if_block.c();
          transition_in(if_block, 1);
          if_block.m(if_block_anchor.parentNode, if_block_anchor);
        }
      } else if (if_block) {
        group_outros();
        transition_out(if_block, 1, 1, function () {
          if_block = null;
        });
        check_outros();
      }
    },
    i: function intro(local) {
      if (current) return;
      transition_in(if_block);
      current = true;
    },
    o: function outro(local) {
      transition_out(if_block);
      current = false;
    },
    d: function destroy(detaching) {
      if (if_block) if_block.d(detaching);
      if (detaching) detach_dev(if_block_anchor);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_each_block.name,
    type: "each",
    source: "(19:2) {#each articles as { url, image, date, title, body }}",
    ctx: ctx
  });
  return block;
}

function create_fragment(ctx) {
  var ul;
  var current;
  var each_value =
  /*articles*/
  ctx[0];
  validate_each_argument(each_value);
  var each_blocks = [];

  for (var i = 0; i < each_value.length; i += 1) {
    each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
  }

  var out = function out(i) {
    return transition_out(each_blocks[i], 1, 1, function () {
      each_blocks[i] = null;
    });
  };

  var block = {
    c: function create() {
      ul = element("ul");

      for (var _i = 0; _i < each_blocks.length; _i += 1) {
        each_blocks[_i].c();
      }

      this.h();
    },
    l: function claim(nodes) {
      ul = claim_element(nodes, "UL", {
        class: true
      });
      var ul_nodes = children(ul);

      for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
        each_blocks[_i2].l(ul_nodes);
      }

      ul_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(ul, "class", "svelte-jpws6w");
      add_location(ul, file, 17, 0, 391);
    },
    m: function mount(target, anchor) {
      insert_dev(target, ul, anchor);

      for (var _i3 = 0; _i3 < each_blocks.length; _i3 += 1) {
        each_blocks[_i3].m(ul, null);
      }

      current = true;
    },
    p: function update(ctx, _ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];

      if (dirty &
      /*articles, ellipsis*/
      3) {
        each_value =
        /*articles*/
        ctx[0];
        validate_each_argument(each_value);

        var _i4;

        for (_i4 = 0; _i4 < each_value.length; _i4 += 1) {
          var child_ctx = get_each_context(ctx, each_value, _i4);

          if (each_blocks[_i4]) {
            each_blocks[_i4].p(child_ctx, dirty);

            transition_in(each_blocks[_i4], 1);
          } else {
            each_blocks[_i4] = create_each_block(child_ctx);

            each_blocks[_i4].c();

            transition_in(each_blocks[_i4], 1);

            each_blocks[_i4].m(ul, null);
          }
        }

        group_outros();

        for (_i4 = each_value.length; _i4 < each_blocks.length; _i4 += 1) {
          out(_i4);
        }

        check_outros();
      }
    },
    i: function intro(local) {
      if (current) return;

      for (var _i5 = 0; _i5 < each_value.length; _i5 += 1) {
        transition_in(each_blocks[_i5]);
      }

      current = true;
    },
    o: function outro(local) {
      each_blocks = each_blocks.filter(Boolean);

      for (var _i6 = 0; _i6 < each_blocks.length; _i6 += 1) {
        transition_out(each_blocks[_i6]);
      }

      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(ul);
      destroy_each(each_blocks, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function instance($$self, $$props, $$invalidate) {
  var articles = $$props.articles;

  var ellipsis = function ellipsis(str, max) {
    if (str.length < max) return str;
    var shortString = str.slice(0, max);

    if (shortString.slice(-1)) {
      return shortString.slice(0, shortString.length - 1) + "...";
    } else {
      return shortString + "...";
    }
  };

  var writable_props = ["articles"];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn("<ArticleList> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("ArticleList", $$slots, []);

  $$self.$set = function ($$props) {
    if ("articles" in $$props) $$invalidate(0, articles = $$props.articles);
  };

  $$self.$capture_state = function () {
    return {
      Icon: Icon,
      Button: Button,
      articles: articles,
      ellipsis: ellipsis
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("articles" in $$props) $$invalidate(0, articles = $$props.articles);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  return [articles, ellipsis];
}

var ArticleList = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(ArticleList, _SvelteComponentDev);

  var _super = _createSuper(ArticleList);

  function ArticleList(options) {
    var _this;

    _classCallCheck(this, ArticleList);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance, create_fragment, safe_not_equal, {
      articles: 0
    });
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "ArticleList",
      options: options,
      id: create_fragment.name
    });
    var ctx = _this.$$.ctx;
    var props = options.props || {};

    if (
    /*articles*/
    ctx[0] === undefined && !("articles" in props)) {
      console.warn("<ArticleList> was created without expected prop 'articles'");
    }

    return _this;
  }

  _createClass(ArticleList, [{
    key: "articles",
    get: function get() {
      throw new Error("<ArticleList>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<ArticleList>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }]);

  return ArticleList;
}(SvelteComponentDev);

function _createSuper$1(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$1(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct$1() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
var file$1 = "src/components/AssetList.svelte";

function get_each_context$1(ctx, list, i) {
  var child_ctx = ctx.slice();
  child_ctx[1] = list[i].title;
  child_ctx[2] = list[i].image;
  child_ctx[3] = list[i].details;
  child_ctx[4] = list[i].url;
  return child_ctx;
} // (15:10) {#if image}


function create_if_block$1(ctx) {
  var div;
  var img;
  var img_src_value;
  var img_alt_value;
  var block = {
    c: function create() {
      div = element("div");
      img = element("img");
      this.h();
    },
    l: function claim(nodes) {
      div = claim_element(nodes, "DIV", {
        class: true
      });
      var div_nodes = children(div);
      img = claim_element(div_nodes, "IMG", {
        src: true,
        alt: true,
        class: true
      });
      div_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      if (img.src !== (img_src_value =
      /*image*/
      ctx[2])) attr_dev(img, "src", img_src_value);
      attr_dev(img, "alt", img_alt_value =
      /*title*/
      ctx[1]);
      attr_dev(img, "class", "svelte-e120po");
      add_location(img, file$1, 16, 14, 266);
      attr_dev(div, "class", "svelte-e120po");
      add_location(div, file$1, 15, 12, 246);
    },
    m: function mount(target, anchor) {
      insert_dev(target, div, anchor);
      append_dev(div, img);
    },
    p: function update(ctx, dirty) {
      if (dirty &
      /*assets*/
      1 && img.src !== (img_src_value =
      /*image*/
      ctx[2])) {
        attr_dev(img, "src", img_src_value);
      }

      if (dirty &
      /*assets*/
      1 && img_alt_value !== (img_alt_value =
      /*title*/
      ctx[1])) {
        attr_dev(img, "alt", img_alt_value);
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(div);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block$1.name,
    type: "if",
    source: "(15:10) {#if image}",
    ctx: ctx
  });
  return block;
} // (11:2) {#each assets as { title, image, details, url }}


function create_each_block$1(ctx) {
  var li;
  var a;
  var figure;
  var t0;
  var figcaption;
  var t1_value =
  /*details*/
  ctx[3] + "";
  var t1;
  var a_href_value;
  var t2;
  var if_block =
  /*image*/
  ctx[2] && create_if_block$1(ctx);
  var block = {
    c: function create() {
      li = element("li");
      a = element("a");
      figure = element("figure");
      if (if_block) if_block.c();
      t0 = space();
      figcaption = element("figcaption");
      t1 = text(t1_value);
      t2 = space();
      this.h();
    },
    l: function claim(nodes) {
      li = claim_element(nodes, "LI", {
        class: true
      });
      var li_nodes = children(li);
      a = claim_element(li_nodes, "A", {
        href: true,
        class: true
      });
      var a_nodes = children(a);
      figure = claim_element(a_nodes, "FIGURE", {
        class: true
      });
      var figure_nodes = children(figure);
      if (if_block) if_block.l(figure_nodes);
      t0 = claim_space(figure_nodes);
      figcaption = claim_element(figure_nodes, "FIGCAPTION", {
        class: true
      });
      var figcaption_nodes = children(figcaption);
      t1 = claim_text(figcaption_nodes, t1_value);
      figcaption_nodes.forEach(detach_dev);
      figure_nodes.forEach(detach_dev);
      a_nodes.forEach(detach_dev);
      t2 = claim_space(li_nodes);
      li_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(figcaption, "class", "small svelte-e120po");
      add_location(figcaption, file$1, 19, 10, 343);
      attr_dev(figure, "class", "svelte-e120po");
      add_location(figure, file$1, 13, 8, 203);
      attr_dev(a, "href", a_href_value =
      /*url*/
      ctx[4]);
      attr_dev(a, "class", "svelte-e120po");
      add_location(a, file$1, 12, 6, 180);
      attr_dev(li, "class", "svelte-e120po");
      add_location(li, file$1, 11, 4, 169);
    },
    m: function mount(target, anchor) {
      insert_dev(target, li, anchor);
      append_dev(li, a);
      append_dev(a, figure);
      if (if_block) if_block.m(figure, null);
      append_dev(figure, t0);
      append_dev(figure, figcaption);
      append_dev(figcaption, t1);
      append_dev(li, t2);
    },
    p: function update(ctx, dirty) {
      if (
      /*image*/
      ctx[2]) {
        if (if_block) {
          if_block.p(ctx, dirty);
        } else {
          if_block = create_if_block$1(ctx);
          if_block.c();
          if_block.m(figure, t0);
        }
      } else if (if_block) {
        if_block.d(1);
        if_block = null;
      }

      if (dirty &
      /*assets*/
      1 && t1_value !== (t1_value =
      /*details*/
      ctx[3] + "")) set_data_dev(t1, t1_value);

      if (dirty &
      /*assets*/
      1 && a_href_value !== (a_href_value =
      /*url*/
      ctx[4])) {
        attr_dev(a, "href", a_href_value);
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(li);
      if (if_block) if_block.d();
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_each_block$1.name,
    type: "each",
    source: "(11:2) {#each assets as { title, image, details, url }}",
    ctx: ctx
  });
  return block;
}

function create_fragment$1(ctx) {
  var article;
  var header;
  var h3;
  var t0;
  var t1;
  var ul;
  var each_value =
  /*assets*/
  ctx[0];
  validate_each_argument(each_value);
  var each_blocks = [];

  for (var i = 0; i < each_value.length; i += 1) {
    each_blocks[i] = create_each_block$1(get_each_context$1(ctx, each_value, i));
  }

  var block = {
    c: function create() {
      article = element("article");
      header = element("header");
      h3 = element("h3");
      t0 = text(
      /*title*/
      ctx[1]);
      t1 = space();
      ul = element("ul");

      for (var _i = 0; _i < each_blocks.length; _i += 1) {
        each_blocks[_i].c();
      }

      this.h();
    },
    l: function claim(nodes) {
      article = claim_element(nodes, "ARTICLE", {
        class: true
      });
      var article_nodes = children(article);
      header = claim_element(article_nodes, "HEADER", {
        class: true
      });
      var header_nodes = children(header);
      h3 = claim_element(header_nodes, "H3", {
        class: true
      });
      var h3_nodes = children(h3);
      t0 = claim_text(h3_nodes,
      /*title*/
      ctx[1]);
      h3_nodes.forEach(detach_dev);
      header_nodes.forEach(detach_dev);
      t1 = claim_space(article_nodes);
      ul = claim_element(article_nodes, "UL", {
        class: true
      });
      var ul_nodes = children(ul);

      for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
        each_blocks[_i2].l(ul_nodes);
      }

      ul_nodes.forEach(detach_dev);
      article_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(h3, "class", "svelte-e120po");
      add_location(h3, file$1, 7, 2, 82);
      attr_dev(header, "class", "svelte-e120po");
      add_location(header, file$1, 6, 0, 71);
      attr_dev(ul, "class", "svelte-e120po");
      add_location(ul, file$1, 9, 0, 109);
      attr_dev(article, "class", "svelte-e120po");
      add_location(article, file$1, 5, 0, 61);
    },
    m: function mount(target, anchor) {
      insert_dev(target, article, anchor);
      append_dev(article, header);
      append_dev(header, h3);
      append_dev(h3, t0);
      append_dev(article, t1);
      append_dev(article, ul);

      for (var _i3 = 0; _i3 < each_blocks.length; _i3 += 1) {
        each_blocks[_i3].m(ul, null);
      }
    },
    p: function update(ctx, _ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];

      if (dirty &
      /*title*/
      2) set_data_dev(t0,
      /*title*/
      ctx[1]);

      if (dirty &
      /*assets*/
      1) {
        each_value =
        /*assets*/
        ctx[0];
        validate_each_argument(each_value);

        var _i4;

        for (_i4 = 0; _i4 < each_value.length; _i4 += 1) {
          var child_ctx = get_each_context$1(ctx, each_value, _i4);

          if (each_blocks[_i4]) {
            each_blocks[_i4].p(child_ctx, dirty);
          } else {
            each_blocks[_i4] = create_each_block$1(child_ctx);

            each_blocks[_i4].c();

            each_blocks[_i4].m(ul, null);
          }
        }

        for (; _i4 < each_blocks.length; _i4 += 1) {
          each_blocks[_i4].d(1);
        }

        each_blocks.length = each_value.length;
      }
    },
    i: noop,
    o: noop,
    d: function destroy(detaching) {
      if (detaching) detach_dev(article);
      destroy_each(each_blocks, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment$1.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function instance$1($$self, $$props, $$invalidate) {
  var assets = $$props.assets;
  var title = $$props.title;
  var writable_props = ["assets", "title"];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn("<AssetList> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("AssetList", $$slots, []);

  $$self.$set = function ($$props) {
    if ("assets" in $$props) $$invalidate(0, assets = $$props.assets);
    if ("title" in $$props) $$invalidate(1, title = $$props.title);
  };

  $$self.$capture_state = function () {
    return {
      assets: assets,
      title: title
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("assets" in $$props) $$invalidate(0, assets = $$props.assets);
    if ("title" in $$props) $$invalidate(1, title = $$props.title);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  return [assets, title];
}

var AssetList = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(AssetList, _SvelteComponentDev);

  var _super = _createSuper$1(AssetList);

  function AssetList(options) {
    var _this;

    _classCallCheck(this, AssetList);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance$1, create_fragment$1, safe_not_equal, {
      assets: 0,
      title: 1
    });
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "AssetList",
      options: options,
      id: create_fragment$1.name
    });
    var ctx = _this.$$.ctx;
    var props = options.props || {};

    if (
    /*assets*/
    ctx[0] === undefined && !("assets" in props)) {
      console.warn("<AssetList> was created without expected prop 'assets'");
    }

    if (
    /*title*/
    ctx[1] === undefined && !("title" in props)) {
      console.warn("<AssetList> was created without expected prop 'title'");
    }

    return _this;
  }

  _createClass(AssetList, [{
    key: "assets",
    get: function get() {
      throw new Error("<AssetList>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<AssetList>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "title",
    get: function get() {
      throw new Error("<AssetList>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<AssetList>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }]);

  return AssetList;
}(SvelteComponentDev);

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper$2(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$2(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct$2() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
var file$2 = "src/routes/media.svelte"; // (45:0) {#if links}

function create_if_block_4(ctx) {
  var h2;
  var t0;
  var t1;
  var articlelist;
  var current;
  articlelist = new ArticleList({
    props: {
      articles:
      /*links*/
      ctx[0]
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      h2 = element("h2");
      t0 = text("In the media");
      t1 = space();
      create_component(articlelist.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      h2 = claim_element(nodes, "H2", {});
      var h2_nodes = children(h2);
      t0 = claim_text(h2_nodes, "In the media");
      h2_nodes.forEach(detach_dev);
      t1 = claim_space(nodes);
      claim_component(articlelist.$$.fragment, nodes);
      this.h();
    },
    h: function hydrate() {
      add_location(h2, file$2, 45, 4, 1006);
    },
    m: function mount(target, anchor) {
      insert_dev(target, h2, anchor);
      append_dev(h2, t0);
      insert_dev(target, t1, anchor);
      mount_component(articlelist, target, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      var articlelist_changes = {};
      if (dirty &
      /*links*/
      1) articlelist_changes.articles =
      /*links*/
      ctx[0];
      articlelist.$set(articlelist_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(articlelist.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(articlelist.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(h2);
      if (detaching) detach_dev(t1);
      destroy_component(articlelist, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_4.name,
    type: "if",
    source: "(45:0) {#if links}",
    ctx: ctx
  });
  return block;
} // (51:0) {#if pressreleases}


function create_if_block_3$1(ctx) {
  var h2;
  var t0;
  var t1;
  var articlelist;
  var current;
  articlelist = new ArticleList({
    props: {
      articles:
      /*pressreleases*/
      ctx[1]
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      h2 = element("h2");
      t0 = text("Wayout News");
      t1 = space();
      create_component(articlelist.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      h2 = claim_element(nodes, "H2", {});
      var h2_nodes = children(h2);
      t0 = claim_text(h2_nodes, "Wayout News");
      h2_nodes.forEach(detach_dev);
      t1 = claim_space(nodes);
      claim_component(articlelist.$$.fragment, nodes);
      this.h();
    },
    h: function hydrate() {
      add_location(h2, file$2, 51, 6, 1099);
    },
    m: function mount(target, anchor) {
      insert_dev(target, h2, anchor);
      append_dev(h2, t0);
      insert_dev(target, t1, anchor);
      mount_component(articlelist, target, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      var articlelist_changes = {};
      if (dirty &
      /*pressreleases*/
      2) articlelist_changes.articles =
      /*pressreleases*/
      ctx[1];
      articlelist.$set(articlelist_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(articlelist.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(articlelist.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(h2);
      if (detaching) detach_dev(t1);
      destroy_component(articlelist, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_3$1.name,
    type: "if",
    source: "(51:0) {#if pressreleases}",
    ctx: ctx
  });
  return block;
} // (42:0) <Container>


function create_default_slot_1(ctx) {
  var h1;
  var t0;
  var t1;
  var t2;
  var if_block1_anchor;
  var current;
  var if_block0 =
  /*links*/
  ctx[0] && create_if_block_4(ctx);
  var if_block1 =
  /*pressreleases*/
  ctx[1] && create_if_block_3$1(ctx);
  var block = {
    c: function create() {
      h1 = element("h1");
      t0 = text(
      /*title*/
      ctx[4]);
      t1 = space();
      if (if_block0) if_block0.c();
      t2 = space();
      if (if_block1) if_block1.c();
      if_block1_anchor = empty();
      this.h();
    },
    l: function claim(nodes) {
      h1 = claim_element(nodes, "H1", {
        class: true
      });
      var h1_nodes = children(h1);
      t0 = claim_text(h1_nodes,
      /*title*/
      ctx[4]);
      h1_nodes.forEach(detach_dev);
      t1 = claim_space(nodes);
      if (if_block0) if_block0.l(nodes);
      t2 = claim_space(nodes);
      if (if_block1) if_block1.l(nodes);
      if_block1_anchor = empty();
      this.h();
    },
    h: function hydrate() {
      attr_dev(h1, "class", "svelte-z20vt6");
      add_location(h1, file$2, 42, 2, 972);
    },
    m: function mount(target, anchor) {
      insert_dev(target, h1, anchor);
      append_dev(h1, t0);
      insert_dev(target, t1, anchor);
      if (if_block0) if_block0.m(target, anchor);
      insert_dev(target, t2, anchor);
      if (if_block1) if_block1.m(target, anchor);
      insert_dev(target, if_block1_anchor, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      if (
      /*links*/
      ctx[0]) {
        if (if_block0) {
          if_block0.p(ctx, dirty);

          if (dirty &
          /*links*/
          1) {
            transition_in(if_block0, 1);
          }
        } else {
          if_block0 = create_if_block_4(ctx);
          if_block0.c();
          transition_in(if_block0, 1);
          if_block0.m(t2.parentNode, t2);
        }
      } else if (if_block0) {
        group_outros();
        transition_out(if_block0, 1, 1, function () {
          if_block0 = null;
        });
        check_outros();
      }

      if (
      /*pressreleases*/
      ctx[1]) {
        if (if_block1) {
          if_block1.p(ctx, dirty);

          if (dirty &
          /*pressreleases*/
          2) {
            transition_in(if_block1, 1);
          }
        } else {
          if_block1 = create_if_block_3$1(ctx);
          if_block1.c();
          transition_in(if_block1, 1);
          if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
        }
      } else if (if_block1) {
        group_outros();
        transition_out(if_block1, 1, 1, function () {
          if_block1 = null;
        });
        check_outros();
      }
    },
    i: function intro(local) {
      if (current) return;
      transition_in(if_block0);
      transition_in(if_block1);
      current = true;
    },
    o: function outro(local) {
      transition_out(if_block0);
      transition_out(if_block1);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(h1);
      if (detaching) detach_dev(t1);
      if (if_block0) if_block0.d(detaching);
      if (detaching) detach_dev(t2);
      if (if_block1) if_block1.d(detaching);
      if (detaching) detach_dev(if_block1_anchor);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot_1.name,
    type: "slot",
    source: "(42:0) <Container>",
    ctx: ctx
  });
  return block;
} // (57:0) {#if images || videos}


function create_if_block$2(ctx) {
  var container;
  var current;
  container = new Container({
    props: {
      wide: true,
      $$slots: {
        default: [create_default_slot$1]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      create_component(container.$$.fragment);
    },
    l: function claim(nodes) {
      claim_component(container.$$.fragment, nodes);
    },
    m: function mount(target, anchor) {
      mount_component(container, target, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      var container_changes = {};

      if (dirty &
      /*$$scope, videos, images*/
      44) {
        container_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      container.$set(container_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(container.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(container.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      destroy_component(container, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block$2.name,
    type: "if",
    source: "(57:0) {#if images || videos}",
    ctx: ctx
  });
  return block;
} // (61:4) {#if images}


function create_if_block_2$1(ctx) {
  var assetlist;
  var current;
  assetlist = new AssetList({
    props: {
      title: "Images",
      assets:
      /*images*/
      ctx[2]
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      create_component(assetlist.$$.fragment);
    },
    l: function claim(nodes) {
      claim_component(assetlist.$$.fragment, nodes);
    },
    m: function mount(target, anchor) {
      mount_component(assetlist, target, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      var assetlist_changes = {};
      if (dirty &
      /*images*/
      4) assetlist_changes.assets =
      /*images*/
      ctx[2];
      assetlist.$set(assetlist_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(assetlist.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(assetlist.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      destroy_component(assetlist, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_2$1.name,
    type: "if",
    source: "(61:4) {#if images}",
    ctx: ctx
  });
  return block;
} // (65:4) {#if videos}


function create_if_block_1$1(ctx) {
  var assetlist;
  var current;
  assetlist = new AssetList({
    props: {
      title: "Videos",
      assets:
      /*videos*/
      ctx[3]
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      create_component(assetlist.$$.fragment);
    },
    l: function claim(nodes) {
      claim_component(assetlist.$$.fragment, nodes);
    },
    m: function mount(target, anchor) {
      mount_component(assetlist, target, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      var assetlist_changes = {};
      if (dirty &
      /*videos*/
      8) assetlist_changes.assets =
      /*videos*/
      ctx[3];
      assetlist.$set(assetlist_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(assetlist.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(assetlist.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      destroy_component(assetlist, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_1$1.name,
    type: "if",
    source: "(65:4) {#if videos}",
    ctx: ctx
  });
  return block;
} // (58:0) <Container wide>


function create_default_slot$1(ctx) {
  var h2;
  var t0;
  var t1;
  var t2;
  var t3;
  var pagecta;
  var current;
  var if_block0 =
  /*images*/
  ctx[2] && create_if_block_2$1(ctx);
  var if_block1 =
  /*videos*/
  ctx[3] && create_if_block_1$1(ctx);
  pagecta = new PageCTA({
    props: {
      links: [{
        link: "about",
        text: "About Wayout"
      }, {
        link: "contact",
        text: "Contact"
      }]
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      h2 = element("h2");
      t0 = text("Media assets");
      t1 = space();
      if (if_block0) if_block0.c();
      t2 = space();
      if (if_block1) if_block1.c();
      t3 = space();
      create_component(pagecta.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      h2 = claim_element(nodes, "H2", {});
      var h2_nodes = children(h2);
      t0 = claim_text(h2_nodes, "Media assets");
      h2_nodes.forEach(detach_dev);
      t1 = claim_space(nodes);
      if (if_block0) if_block0.l(nodes);
      t2 = claim_space(nodes);
      if (if_block1) if_block1.l(nodes);
      t3 = claim_space(nodes);
      claim_component(pagecta.$$.fragment, nodes);
      this.h();
    },
    h: function hydrate() {
      add_location(h2, file$2, 58, 4, 1229);
    },
    m: function mount(target, anchor) {
      insert_dev(target, h2, anchor);
      append_dev(h2, t0);
      insert_dev(target, t1, anchor);
      if (if_block0) if_block0.m(target, anchor);
      insert_dev(target, t2, anchor);
      if (if_block1) if_block1.m(target, anchor);
      insert_dev(target, t3, anchor);
      mount_component(pagecta, target, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      if (
      /*images*/
      ctx[2]) {
        if (if_block0) {
          if_block0.p(ctx, dirty);

          if (dirty &
          /*images*/
          4) {
            transition_in(if_block0, 1);
          }
        } else {
          if_block0 = create_if_block_2$1(ctx);
          if_block0.c();
          transition_in(if_block0, 1);
          if_block0.m(t2.parentNode, t2);
        }
      } else if (if_block0) {
        group_outros();
        transition_out(if_block0, 1, 1, function () {
          if_block0 = null;
        });
        check_outros();
      }

      if (
      /*videos*/
      ctx[3]) {
        if (if_block1) {
          if_block1.p(ctx, dirty);

          if (dirty &
          /*videos*/
          8) {
            transition_in(if_block1, 1);
          }
        } else {
          if_block1 = create_if_block_1$1(ctx);
          if_block1.c();
          transition_in(if_block1, 1);
          if_block1.m(t3.parentNode, t3);
        }
      } else if (if_block1) {
        group_outros();
        transition_out(if_block1, 1, 1, function () {
          if_block1 = null;
        });
        check_outros();
      }
    },
    i: function intro(local) {
      if (current) return;
      transition_in(if_block0);
      transition_in(if_block1);
      transition_in(pagecta.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(if_block0);
      transition_out(if_block1);
      transition_out(pagecta.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(h2);
      if (detaching) detach_dev(t1);
      if (if_block0) if_block0.d(detaching);
      if (detaching) detach_dev(t2);
      if (if_block1) if_block1.d(detaching);
      if (detaching) detach_dev(t3);
      destroy_component(pagecta, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot$1.name,
    type: "slot",
    source: "(58:0) <Container wide>",
    ctx: ctx
  });
  return block;
}

function create_fragment$2(ctx) {
  var title_value;
  var t0;
  var container;
  var t1;
  var if_block_anchor;
  var current;
  document.title = title_value = "Wayout - " +
  /*title*/
  ctx[4];
  container = new Container({
    props: {
      $$slots: {
        default: [create_default_slot_1]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var if_block = (
  /*images*/
  ctx[2] ||
  /*videos*/
  ctx[3]) && create_if_block$2(ctx);
  var block = {
    c: function create() {
      t0 = space();
      create_component(container.$$.fragment);
      t1 = space();
      if (if_block) if_block.c();
      if_block_anchor = empty();
    },
    l: function claim(nodes) {
      var head_nodes = query_selector_all("[data-svelte=\"svelte-1ptlpyn\"]", document.head);
      head_nodes.forEach(detach_dev);
      t0 = claim_space(nodes);
      claim_component(container.$$.fragment, nodes);
      t1 = claim_space(nodes);
      if (if_block) if_block.l(nodes);
      if_block_anchor = empty();
    },
    m: function mount(target, anchor) {
      insert_dev(target, t0, anchor);
      mount_component(container, target, anchor);
      insert_dev(target, t1, anchor);
      if (if_block) if_block.m(target, anchor);
      insert_dev(target, if_block_anchor, anchor);
      current = true;
    },
    p: function update(ctx, _ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];

      if ((!current || dirty &
      /*title*/
      16) && title_value !== (title_value = "Wayout - " +
      /*title*/
      ctx[4])) {
        document.title = title_value;
      }

      var container_changes = {};

      if (dirty &
      /*$$scope, pressreleases, links*/
      35) {
        container_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      container.$set(container_changes);

      if (
      /*images*/
      ctx[2] ||
      /*videos*/
      ctx[3]) {
        if (if_block) {
          if_block.p(ctx, dirty);

          if (dirty &
          /*images, videos*/
          12) {
            transition_in(if_block, 1);
          }
        } else {
          if_block = create_if_block$2(ctx);
          if_block.c();
          transition_in(if_block, 1);
          if_block.m(if_block_anchor.parentNode, if_block_anchor);
        }
      } else if (if_block) {
        group_outros();
        transition_out(if_block, 1, 1, function () {
          if_block = null;
        });
        check_outros();
      }
    },
    i: function intro(local) {
      if (current) return;
      transition_in(container.$$.fragment, local);
      transition_in(if_block);
      current = true;
    },
    o: function outro(local) {
      transition_out(container.$$.fragment, local);
      transition_out(if_block);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(t0);
      destroy_component(container, detaching);
      if (detaching) detach_dev(t1);
      if (if_block) if_block.d(detaching);
      if (detaching) detach_dev(if_block_anchor);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment$2.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function preload() {
  return _preload.apply(this, arguments);
}

function _preload() {
  _preload = _asyncToGenerator( /*#__PURE__*/regenerator.mark(function _callee() {
    var res, data;
    return regenerator.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return this.fetch("media.json");

          case 2:
            res = _context.sent;
            _context.next = 5;
            return res.json();

          case 5:
            data = _context.sent;

            if (!(res.status === 200)) {
              _context.next = 10;
              break;
            }

            return _context.abrupt("return", _objectSpread({}, data));

          case 10:
            this.error(res.status, data.message);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _preload.apply(this, arguments);
}

function instance$2($$self, $$props, $$invalidate) {
  var links = $$props.links;
  var pressreleases = $$props.pressreleases;
  var images = $$props.images;
  var videos = $$props.videos;
  var title = "Press & Media";
  links.sort(function (a, b) {
    if (a.date === null) return 1;
    var keyA = new Date(a.date),
        keyB = new Date(b.date); // Compare the 2 dates

    if (keyA < keyB) return 1;
    if (keyA > keyB) return -1;
    return 0;
  });
  var writable_props = ["links", "pressreleases", "images", "videos"];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn("<Media> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("Media", $$slots, []);

  $$self.$set = function ($$props) {
    if ("links" in $$props) $$invalidate(0, links = $$props.links);
    if ("pressreleases" in $$props) $$invalidate(1, pressreleases = $$props.pressreleases);
    if ("images" in $$props) $$invalidate(2, images = $$props.images);
    if ("videos" in $$props) $$invalidate(3, videos = $$props.videos);
  };

  $$self.$capture_state = function () {
    return {
      preload: preload,
      ArticleList: ArticleList,
      AssetList: AssetList,
      Container: Container,
      PageCTA: PageCTA,
      links: links,
      pressreleases: pressreleases,
      images: images,
      videos: videos,
      title: title
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("links" in $$props) $$invalidate(0, links = $$props.links);
    if ("pressreleases" in $$props) $$invalidate(1, pressreleases = $$props.pressreleases);
    if ("images" in $$props) $$invalidate(2, images = $$props.images);
    if ("videos" in $$props) $$invalidate(3, videos = $$props.videos);
    if ("title" in $$props) $$invalidate(4, title = $$props.title);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  return [links, pressreleases, images, videos, title];
}

var Media = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(Media, _SvelteComponentDev);

  var _super = _createSuper$2(Media);

  function Media(options) {
    var _this;

    _classCallCheck(this, Media);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance$2, create_fragment$2, safe_not_equal, {
      links: 0,
      pressreleases: 1,
      images: 2,
      videos: 3
    });
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "Media",
      options: options,
      id: create_fragment$2.name
    });
    var ctx = _this.$$.ctx;
    var props = options.props || {};

    if (
    /*links*/
    ctx[0] === undefined && !("links" in props)) {
      console.warn("<Media> was created without expected prop 'links'");
    }

    if (
    /*pressreleases*/
    ctx[1] === undefined && !("pressreleases" in props)) {
      console.warn("<Media> was created without expected prop 'pressreleases'");
    }

    if (
    /*images*/
    ctx[2] === undefined && !("images" in props)) {
      console.warn("<Media> was created without expected prop 'images'");
    }

    if (
    /*videos*/
    ctx[3] === undefined && !("videos" in props)) {
      console.warn("<Media> was created without expected prop 'videos'");
    }

    return _this;
  }

  _createClass(Media, [{
    key: "links",
    get: function get() {
      throw new Error("<Media>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Media>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "pressreleases",
    get: function get() {
      throw new Error("<Media>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Media>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "images",
    get: function get() {
      throw new Error("<Media>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Media>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "videos",
    get: function get() {
      throw new Error("<Media>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Media>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }]);

  return Media;
}(SvelteComponentDev);

export default Media;
export { preload };
