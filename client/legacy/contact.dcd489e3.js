import { ae as identity, w as writable, c as _inherits, d as _getPrototypeOf, e as _possibleConstructorReturn, f as _classCallCheck, i as init, s as safe_not_equal, g as _assertThisInitialized, h as dispatch_dev, S as SvelteComponentDev, Y as create_slot, o as onDestroy, v as validate_slots, Z as createEventDispatcher, l as element, m as space, n as claim_element, p as children, q as detach_dev, t as claim_space, u as attr_dev, x as add_location, z as insert_dev, A as append_dev, D as listen_dev, b as _slicedToArray, a2 as update_slot, k as add_render_callback, af as create_in_transition, R as transition_in, ag as create_out_transition, T as transition_out, H as run_all, I as binding_callbacks, a as _asyncToGenerator, r as regenerator, j as _createClass, a4 as validate_each_argument, K as Icon, J as globals, L as text, N as claim_text, B as toggle_class, Q as set_data_dev, M as create_component, O as claim_component, P as mount_component, ah as prevent_default, aa as destroy_each, U as destroy_component, ai as to_number, aj as select_value, ak as prop_dev, al as set_input_value, am as select_option, ac as empty, $ as validate_store, a0 as component_subscribe, ad as query_selector_all, a8 as group_outros, a9 as check_outros, G as noop } from './client.dc955f77.js';
import { _ as _defineProperty } from './defineProperty.bdf5641a.js';
import { C as Container } from './Container.19136de0.js';
import { B as Button, P as PageCTA } from './PageCTA.ddb524e9.js';

function cubicOut(t) {
  var f = t - 1.0;
  return f * f * f + 1.0;
}

function expoIn(t) {
  return t === 0.0 ? t : Math.pow(2.0, 10.0 * (t - 1.0));
}

function expoOut(t) {
  return t === 1.0 ? t : 1.0 - Math.pow(2.0, -10.0 * t);
}

function sineInOut(t) {
  return -0.5 * (Math.cos(Math.PI * t) - 1);
}

function fade(node, _ref2) {
  var _ref2$delay = _ref2.delay,
      delay = _ref2$delay === void 0 ? 0 : _ref2$delay,
      _ref2$duration = _ref2.duration,
      duration = _ref2$duration === void 0 ? 400 : _ref2$duration,
      _ref2$easing = _ref2.easing,
      easing = _ref2$easing === void 0 ? identity : _ref2$easing;
  var o = +getComputedStyle(node).opacity;
  return {
    delay: delay,
    duration: duration,
    easing: easing,
    css: function css(t) {
      return "opacity: ".concat(t * o);
    }
  };
}

function fly(node, _ref3) {
  var _ref3$delay = _ref3.delay,
      delay = _ref3$delay === void 0 ? 0 : _ref3$delay,
      _ref3$duration = _ref3.duration,
      duration = _ref3$duration === void 0 ? 400 : _ref3$duration,
      _ref3$easing = _ref3.easing,
      easing = _ref3$easing === void 0 ? cubicOut : _ref3$easing,
      _ref3$x = _ref3.x,
      x = _ref3$x === void 0 ? 0 : _ref3$x,
      _ref3$y = _ref3.y,
      y = _ref3$y === void 0 ? 0 : _ref3$y,
      _ref3$opacity = _ref3.opacity,
      opacity = _ref3$opacity === void 0 ? 0 : _ref3$opacity;
  var style = getComputedStyle(node);
  var target_opacity = +style.opacity;
  var transform = style.transform === 'none' ? '' : style.transform;
  var od = target_opacity * (1 - opacity);
  return {
    delay: delay,
    duration: duration,
    easing: easing,
    css: function css(t, u) {
      return "\n\t\t\ttransform: ".concat(transform, " translate(").concat((1 - t) * x, "px, ").concat((1 - t) * y, "px);\n\t\t\topacity: ").concat(target_opacity - od * u);
    }
  };
}

var modalVisible = writable(false);

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
var file = "src/components/Modal/Modal.svelte";

function create_fragment(ctx) {
  var div3;
  var div0;
  var div0_intro;
  var div0_outro;
  var t0;
  var div2;
  var div1;
  var button;
  var t1;
  var div1_outro;
  var div2_intro;
  var current;
  var mounted;
  var dispose;
  var default_slot_template =
  /*$$slots*/
  ctx[5].default;
  var default_slot = create_slot(default_slot_template, ctx,
  /*$$scope*/
  ctx[4], null);
  var block = {
    c: function create() {
      div3 = element("div");
      div0 = element("div");
      t0 = space();
      div2 = element("div");
      div1 = element("div");
      button = element("button");
      t1 = space();
      if (default_slot) default_slot.c();
      this.h();
    },
    l: function claim(nodes) {
      div3 = claim_element(nodes, "DIV", {
        class: true
      });
      var div3_nodes = children(div3);
      div0 = claim_element(div3_nodes, "DIV", {
        class: true
      });
      children(div0).forEach(detach_dev);
      t0 = claim_space(div3_nodes);
      div2 = claim_element(div3_nodes, "DIV", {
        class: true,
        role: true,
        "aria-modal": true
      });
      var div2_nodes = children(div2);
      div1 = claim_element(div2_nodes, "DIV", {
        class: true
      });
      var div1_nodes = children(div1);
      button = claim_element(div1_nodes, "BUTTON", {
        class: true
      });
      children(button).forEach(detach_dev);
      t1 = claim_space(div1_nodes);
      if (default_slot) default_slot.l(div1_nodes);
      div1_nodes.forEach(detach_dev);
      div2_nodes.forEach(detach_dev);
      div3_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(div0, "class", "modal-background svelte-j11j91");
      add_location(div0, file, 68, 2, 1589);
      attr_dev(button, "class", "close svelte-j11j91");
      add_location(button, file, 79, 6, 1887);
      attr_dev(div1, "class", "modal-inner svelte-j11j91");
      add_location(div1, file, 78, 4, 1841);
      attr_dev(div2, "class", "modal svelte-j11j91");
      attr_dev(div2, "role", "dialog");
      attr_dev(div2, "aria-modal", "true");
      add_location(div2, file, 77, 2, 1758);
      attr_dev(div3, "class", "modal-wrapper svelte-j11j91");
      add_location(div3, file, 67, 0, 1559);
    },
    m: function mount(target, anchor) {
      insert_dev(target, div3, anchor);
      append_dev(div3, div0);
      append_dev(div3, t0);
      append_dev(div3, div2);
      append_dev(div2, div1);
      append_dev(div1, button);
      append_dev(div1, t1);

      if (default_slot) {
        default_slot.m(div1, null);
      }
      /*div2_binding*/


      ctx[8](div2);
      current = true;

      if (!mounted) {
        dispose = [listen_dev(window, "keydown",
        /*handleKeydown*/
        ctx[1], false, false, false), listen_dev(div0, "click",
        /*click_handler*/
        ctx[6], false, false, false), listen_dev(button, "click",
        /*click_handler_1*/
        ctx[7], false, false, false)];
        mounted = true;
      }
    },
    p: function update(ctx, _ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];

      if (default_slot) {
        if (default_slot.p && dirty &
        /*$$scope*/
        16) {
          update_slot(default_slot, default_slot_template, ctx,
          /*$$scope*/
          ctx[4], dirty, null, null);
        }
      }
    },
    i: function intro(local) {
      if (current) return;
      add_render_callback(function () {
        if (div0_outro) div0_outro.end(1);
        if (!div0_intro) div0_intro = create_in_transition(div0, fade, {
          duration: 300
        });
        div0_intro.start();
      });
      transition_in(default_slot, local);
      if (div1_outro) div1_outro.end(1);

      if (!div2_intro) {
        add_render_callback(function () {
          div2_intro = create_in_transition(div2,
          /*scale*/
          ctx[2], {});
          div2_intro.start();
        });
      }

      current = true;
    },
    o: function outro(local) {
      if (div0_intro) div0_intro.invalidate();
      div0_outro = create_out_transition(div0, fade, {
        duration: 300
      });
      transition_out(default_slot, local);
      div1_outro = create_out_transition(div1,
      /*leaveDown*/
      ctx[3], {});
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(div3);
      if (detaching && div0_outro) div0_outro.end();
      if (default_slot) default_slot.d(detaching);
      if (detaching && div1_outro) div1_outro.end();
      /*div2_binding*/

      ctx[8](null);
      mounted = false;
      run_all(dispose);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function instance($$self, $$props, $$invalidate) {
  var modal;

  var handleKeydown = function handleKeydown(e) {
    if (e.key === "Escape") {
      close();
      return;
    }

    if (e.key === "Tab") {
      // trap focus
      var nodes = modal.querySelectorAll("*");
      var tabbable = Array.from(nodes).filter(function (n) {
        return n.tabIndex >= 0;
      });
      var index = tabbable.indexOf(document.activeElement);
      if (index === -1 && e.shiftKey) index = 0;
      index += tabbable.length + (e.shiftKey ? -1 : 1);
      index %= tabbable.length;
      tabbable[index].focus();
      e.preventDefault();
    }
  };

  var previouslyFocused = typeof document !== "undefined" && document.activeElement;
  onDestroy(function () {
    if (previouslyFocused) {
      previouslyFocused.focus();
    }
  });

  var scale = function scale(node) {
    var existingTransform = getComputedStyle(node).transform.replace("none", "");
    return {
      duration: 1000,
      easing: expoOut,
      css: function css(t, u) {
        return "transform: ".concat(existingTransform, " scale(").concat(0.98 + t * 0.02, "); opacity: ").concat(t);
      }
    };
  };

  var leaveDown = function leaveDown() {
    return {
      duration: 300,
      easing: sineInOut,
      css: function css(t, u) {
        return "transform: translateY(".concat(1 * u, "%) scale(").concat(0.99 + t * 0.01, "); opacity: ").concat(t);
      }
    };
  };

  var writable_props = [];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn("<Modal> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("Modal", $$slots, ['default']);

  var click_handler = function click_handler() {
    modalVisible.set(false);
  };

  var click_handler_1 = function click_handler_1() {
    modalVisible.set(false);
  };

  function div2_binding($$value) {
    binding_callbacks[$$value ? "unshift" : "push"](function () {
      modal = $$value;
      $$invalidate(0, modal);
    });
  }

  $$self.$set = function ($$props) {
    if ("$$scope" in $$props) $$invalidate(4, $$scope = $$props.$$scope);
  };

  $$self.$capture_state = function () {
    return {
      createEventDispatcher: createEventDispatcher,
      onDestroy: onDestroy,
      fade: fade,
      fly: fly,
      expoOut: expoOut,
      sineInOut: sineInOut,
      showModalStore: modalVisible,
      modal: modal,
      handleKeydown: handleKeydown,
      previouslyFocused: previouslyFocused,
      scale: scale,
      leaveDown: leaveDown
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("modal" in $$props) $$invalidate(0, modal = $$props.modal);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  return [modal, handleKeydown, scale, leaveDown, $$scope, $$slots, click_handler, click_handler_1, div2_binding];
}

var Modal = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(Modal, _SvelteComponentDev);

  var _super = _createSuper(Modal);

  function Modal(options) {
    var _this;

    _classCallCheck(this, Modal);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance, create_fragment, safe_not_equal, {});
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "Modal",
      options: options,
      id: create_fragment.name
    });
    return _this;
  }

  return Modal;
}(SvelteComponentDev);

var email = function email(val, args) {
  var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return val && regex.test(val);
};

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var defaultOptions = {
  method: "post",
  mode: "same-origin",
  cache: "no-cache",
  credentials: "same-origin",
  headers: {
    "content-type": "application/json"
  },
  redirect: "follow",
  referrerpolicy: "origin"
};

var post = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regenerator.mark(function _callee() {
    var url,
        data,
        options,
        response,
        _args = arguments;
    return regenerator.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            url = _args.length > 0 && _args[0] !== undefined ? _args[0] : "";
            data = _args.length > 1 && _args[1] !== undefined ? _args[1] : {};
            options = _args.length > 2 && _args[2] !== undefined ? _args[2] : {};
            _context.next = 5;
            return fetch(url, _objectSpread(_objectSpread(_objectSpread({}, defaultOptions), options), {}, {
              body: JSON.stringify(data)
            }));

          case 5:
            response = _context.sent;
            return _context.abrupt("return", response.json());

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function post() {
    return _ref.apply(this, arguments);
  };
}();

function _createSuper$1(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$1(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct$1() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys$1(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread$1(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys$1(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys$1(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }
var console_1 = globals.console;
var file$1 = "src/components/LeadForm.svelte";

function get_each_context_1(ctx, list, i) {
  var child_ctx = ctx.slice();
  child_ctx[28] = list[i];
  return child_ctx;
}

function get_each_context(ctx, list, i) {
  var child_ctx = ctx.slice();
  child_ctx[21] = list[i].id;
  child_ctx[22] = list[i].label;
  child_ctx[23] = list[i].type;
  child_ctx[24] = list[i].options;
  child_ctx[25] = list[i].fullWidth;
  child_ctx[27] = i;
  return child_ctx;
} // (116:40) 


function create_if_block_6(ctx) {
  var textarea;
  var textarea_id_value;
  var textarea_name_value;
  var mounted;
  var dispose;

  function textarea_input_handler() {
    /*textarea_input_handler*/
    ctx[14].call(textarea,
    /*id*/
    ctx[21]);
  }

  var block = {
    c: function create() {
      textarea = element("textarea");
      this.h();
    },
    l: function claim(nodes) {
      textarea = claim_element(nodes, "TEXTAREA", {
        maxlength: true,
        id: true,
        name: true,
        class: true
      });
      children(textarea).forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(textarea, "maxlength", "140");
      attr_dev(textarea, "id", textarea_id_value =
      /*id*/
      ctx[21]);
      attr_dev(textarea, "name", textarea_name_value =
      /*id*/
      ctx[21]);
      attr_dev(textarea, "class", "svelte-100zhid");
      add_location(textarea, file$1, 116, 12, 2978);
    },
    m: function mount(target, anchor) {
      insert_dev(target, textarea, anchor);
      set_input_value(textarea,
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]]);

      if (!mounted) {
        dispose = listen_dev(textarea, "input", textarea_input_handler);
        mounted = true;
      }
    },
    p: function update(new_ctx, dirty) {
      ctx = new_ctx;

      if (dirty[0] &
      /*config*/
      1 && textarea_id_value !== (textarea_id_value =
      /*id*/
      ctx[21])) {
        attr_dev(textarea, "id", textarea_id_value);
      }

      if (dirty[0] &
      /*config*/
      1 && textarea_name_value !== (textarea_name_value =
      /*id*/
      ctx[21])) {
        attr_dev(textarea, "name", textarea_name_value);
      }

      if (dirty[0] &
      /*fields, config*/
      65) {
        set_input_value(textarea,
        /*fields*/
        ctx[6][
        /*id*/
        ctx[21]]);
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(textarea);
      mounted = false;
      dispose();
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_6.name,
    type: "if",
    source: "(116:40) ",
    ctx: ctx
  });
  return block;
} // (107:38) 


function create_if_block_5(ctx) {
  var div;
  var select;
  var option;
  var select_id_value;
  var select_name_value;
  var mounted;
  var dispose;
  var each_value_1 =
  /*options*/
  ctx[24];
  validate_each_argument(each_value_1);
  var each_blocks = [];

  for (var i = 0; i < each_value_1.length; i += 1) {
    each_blocks[i] = create_each_block_1(get_each_context_1(ctx, each_value_1, i));
  }

  function select_change_handler() {
    /*select_change_handler*/
    ctx[13].call(select,
    /*id*/
    ctx[21]);
  }

  var block = {
    c: function create() {
      div = element("div");
      select = element("select");
      option = element("option");

      for (var _i = 0; _i < each_blocks.length; _i += 1) {
        each_blocks[_i].c();
      }

      this.h();
    },
    l: function claim(nodes) {
      div = claim_element(nodes, "DIV", {
        class: true
      });
      var div_nodes = children(div);
      select = claim_element(div_nodes, "SELECT", {
        id: true,
        name: true,
        class: true
      });
      var select_nodes = children(select);
      option = claim_element(select_nodes, "OPTION", {
        value: true
      });
      children(option).forEach(detach_dev);

      for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
        each_blocks[_i2].l(select_nodes);
      }

      select_nodes.forEach(detach_dev);
      div_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      option.__value = "none";
      option.value = option.__value;
      add_location(option, file$1, 109, 16, 2733);
      attr_dev(select, "id", select_id_value =
      /*id*/
      ctx[21]);
      attr_dev(select, "name", select_name_value =
      /*id*/
      ctx[21]);
      attr_dev(select, "class", "svelte-100zhid");
      if (
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]] === void 0) add_render_callback(select_change_handler);
      add_location(select, file$1, 108, 14, 2669);
      attr_dev(div, "class", "select-wrapper");
      add_location(div, file$1, 107, 12, 2626);
    },
    m: function mount(target, anchor) {
      insert_dev(target, div, anchor);
      append_dev(div, select);
      append_dev(select, option);

      for (var _i3 = 0; _i3 < each_blocks.length; _i3 += 1) {
        each_blocks[_i3].m(select, null);
      }

      select_option(select,
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]]);

      if (!mounted) {
        dispose = listen_dev(select, "change", select_change_handler);
        mounted = true;
      }
    },
    p: function update(new_ctx, dirty) {
      ctx = new_ctx;

      if (dirty[0] &
      /*config*/
      1) {
        each_value_1 =
        /*options*/
        ctx[24];
        validate_each_argument(each_value_1);

        var _i4;

        for (_i4 = 0; _i4 < each_value_1.length; _i4 += 1) {
          var child_ctx = get_each_context_1(ctx, each_value_1, _i4);

          if (each_blocks[_i4]) {
            each_blocks[_i4].p(child_ctx, dirty);
          } else {
            each_blocks[_i4] = create_each_block_1(child_ctx);

            each_blocks[_i4].c();

            each_blocks[_i4].m(select, null);
          }
        }

        for (; _i4 < each_blocks.length; _i4 += 1) {
          each_blocks[_i4].d(1);
        }

        each_blocks.length = each_value_1.length;
      }

      if (dirty[0] &
      /*config*/
      1 && select_id_value !== (select_id_value =
      /*id*/
      ctx[21])) {
        attr_dev(select, "id", select_id_value);
      }

      if (dirty[0] &
      /*config*/
      1 && select_name_value !== (select_name_value =
      /*id*/
      ctx[21])) {
        attr_dev(select, "name", select_name_value);
      }

      if (dirty[0] &
      /*fields, config*/
      65) {
        select_option(select,
        /*fields*/
        ctx[6][
        /*id*/
        ctx[21]]);
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(div);
      destroy_each(each_blocks, detaching);
      mounted = false;
      dispose();
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_5.name,
    type: "if",
    source: "(107:38) ",
    ctx: ctx
  });
  return block;
} // (105:38) 


function create_if_block_4(ctx) {
  var input;
  var input_id_value;
  var input_name_value;
  var mounted;
  var dispose;

  function input_input_handler_2() {
    /*input_input_handler_2*/
    ctx[12].call(input,
    /*id*/
    ctx[21]);
  }

  var block = {
    c: function create() {
      input = element("input");
      this.h();
    },
    l: function claim(nodes) {
      input = claim_element(nodes, "INPUT", {
        type: true,
        id: true,
        name: true,
        class: true
      });
      this.h();
    },
    h: function hydrate() {
      attr_dev(input, "type", "number");
      attr_dev(input, "id", input_id_value =
      /*id*/
      ctx[21]);
      attr_dev(input, "name", input_name_value =
      /*id*/
      ctx[21]);
      attr_dev(input, "class", "svelte-100zhid");
      add_location(input, file$1, 105, 12, 2512);
    },
    m: function mount(target, anchor) {
      insert_dev(target, input, anchor);
      set_input_value(input,
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]]);

      if (!mounted) {
        dispose = listen_dev(input, "input", input_input_handler_2);
        mounted = true;
      }
    },
    p: function update(new_ctx, dirty) {
      ctx = new_ctx;

      if (dirty[0] &
      /*config*/
      1 && input_id_value !== (input_id_value =
      /*id*/
      ctx[21])) {
        attr_dev(input, "id", input_id_value);
      }

      if (dirty[0] &
      /*config*/
      1 && input_name_value !== (input_name_value =
      /*id*/
      ctx[21])) {
        attr_dev(input, "name", input_name_value);
      }

      if (dirty[0] &
      /*fields, config*/
      65 && to_number(input.value) !==
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]]) {
        set_input_value(input,
        /*fields*/
        ctx[6][
        /*id*/
        ctx[21]]);
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(input);
      mounted = false;
      dispose();
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_4.name,
    type: "if",
    source: "(105:38) ",
    ctx: ctx
  });
  return block;
} // (103:36) 


function create_if_block_3(ctx) {
  var input;
  var input_id_value;
  var input_name_value;
  var mounted;
  var dispose;

  function input_input_handler_1() {
    /*input_input_handler_1*/
    ctx[11].call(input,
    /*id*/
    ctx[21]);
  }

  var block = {
    c: function create() {
      input = element("input");
      this.h();
    },
    l: function claim(nodes) {
      input = claim_element(nodes, "INPUT", {
        type: true,
        id: true,
        name: true,
        class: true
      });
      this.h();
    },
    h: function hydrate() {
      attr_dev(input, "type", "text");
      attr_dev(input, "id", input_id_value =
      /*id*/
      ctx[21]);
      attr_dev(input, "name", input_name_value =
      /*id*/
      ctx[21]);
      attr_dev(input, "class", "svelte-100zhid");
      add_location(input, file$1, 103, 12, 2400);
    },
    m: function mount(target, anchor) {
      insert_dev(target, input, anchor);
      set_input_value(input,
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]]);

      if (!mounted) {
        dispose = listen_dev(input, "input", input_input_handler_1);
        mounted = true;
      }
    },
    p: function update(new_ctx, dirty) {
      ctx = new_ctx;

      if (dirty[0] &
      /*config*/
      1 && input_id_value !== (input_id_value =
      /*id*/
      ctx[21])) {
        attr_dev(input, "id", input_id_value);
      }

      if (dirty[0] &
      /*config*/
      1 && input_name_value !== (input_name_value =
      /*id*/
      ctx[21])) {
        attr_dev(input, "name", input_name_value);
      }

      if (dirty[0] &
      /*fields, config*/
      65 && input.value !==
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]]) {
        set_input_value(input,
        /*fields*/
        ctx[6][
        /*id*/
        ctx[21]]);
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(input);
      mounted = false;
      dispose();
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_3.name,
    type: "if",
    source: "(103:36) ",
    ctx: ctx
  });
  return block;
} // (92:10) {#if id === 'email'}


function create_if_block_1(ctx) {
  var input;
  var input_id_value;
  var input_name_value;
  var t;
  var show_if =
  /*emailTouched*/
  ctx[2] && !email(
  /*fields*/
  ctx[6][
  /*id*/
  ctx[21]]);
  var if_block_anchor;
  var mounted;
  var dispose;

  function input_input_handler() {
    /*input_input_handler*/
    ctx[10].call(input,
    /*id*/
    ctx[21]);
  }

  var if_block = show_if && create_if_block_2(ctx);
  var block = {
    c: function create() {
      input = element("input");
      t = space();
      if (if_block) if_block.c();
      if_block_anchor = empty();
      this.h();
    },
    l: function claim(nodes) {
      input = claim_element(nodes, "INPUT", {
        id: true,
        name: true,
        type: true,
        class: true
      });
      t = claim_space(nodes);
      if (if_block) if_block.l(nodes);
      if_block_anchor = empty();
      this.h();
    },
    h: function hydrate() {
      attr_dev(input, "id", input_id_value =
      /*id*/
      ctx[21]);
      attr_dev(input, "name", input_name_value =
      /*id*/
      ctx[21]);
      attr_dev(input, "type", "text");
      attr_dev(input, "class", "svelte-100zhid");
      add_location(input, file$1, 92, 12, 2053);
    },
    m: function mount(target, anchor) {
      insert_dev(target, input, anchor);
      set_input_value(input,
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]]);
      insert_dev(target, t, anchor);
      if (if_block) if_block.m(target, anchor);
      insert_dev(target, if_block_anchor, anchor);

      if (!mounted) {
        dispose = [listen_dev(input, "blur",
        /*touchEmail*/
        ctx[7], false, false, false), listen_dev(input, "input", input_input_handler)];
        mounted = true;
      }
    },
    p: function update(new_ctx, dirty) {
      ctx = new_ctx;

      if (dirty[0] &
      /*config*/
      1 && input_id_value !== (input_id_value =
      /*id*/
      ctx[21])) {
        attr_dev(input, "id", input_id_value);
      }

      if (dirty[0] &
      /*config*/
      1 && input_name_value !== (input_name_value =
      /*id*/
      ctx[21])) {
        attr_dev(input, "name", input_name_value);
      }

      if (dirty[0] &
      /*fields, config*/
      65 && input.value !==
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]]) {
        set_input_value(input,
        /*fields*/
        ctx[6][
        /*id*/
        ctx[21]]);
      }

      if (dirty[0] &
      /*emailTouched, fields, config*/
      69) show_if =
      /*emailTouched*/
      ctx[2] && !email(
      /*fields*/
      ctx[6][
      /*id*/
      ctx[21]]);

      if (show_if) {
        if (if_block) ; else {
          if_block = create_if_block_2(ctx);
          if_block.c();
          if_block.m(if_block_anchor.parentNode, if_block_anchor);
        }
      } else if (if_block) {
        if_block.d(1);
        if_block = null;
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(input);
      if (detaching) detach_dev(t);
      if (if_block) if_block.d(detaching);
      if (detaching) detach_dev(if_block_anchor);
      mounted = false;
      run_all(dispose);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_1.name,
    type: "if",
    source: "(92:10) {#if id === 'email'}",
    ctx: ctx
  });
  return block;
} // (111:16) {#each options as option}


function create_each_block_1(ctx) {
  var option;
  var t_value =
  /*option*/
  ctx[28] + "";
  var t;
  var option_value_value;
  var block = {
    c: function create() {
      option = element("option");
      t = text(t_value);
      this.h();
    },
    l: function claim(nodes) {
      option = claim_element(nodes, "OPTION", {
        value: true
      });
      var option_nodes = children(option);
      t = claim_text(option_nodes, t_value);
      option_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      option.__value = option_value_value =
      /*option*/
      ctx[28];
      option.value = option.__value;
      add_location(option, file$1, 111, 18, 2817);
    },
    m: function mount(target, anchor) {
      insert_dev(target, option, anchor);
      append_dev(option, t);
    },
    p: function update(ctx, dirty) {
      if (dirty[0] &
      /*config*/
      1 && t_value !== (t_value =
      /*option*/
      ctx[28] + "")) set_data_dev(t, t_value);

      if (dirty[0] &
      /*config*/
      1 && option_value_value !== (option_value_value =
      /*option*/
      ctx[28])) {
        prop_dev(option, "__value", option_value_value);
      }

      option.value = option.__value;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(option);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_each_block_1.name,
    type: "each",
    source: "(111:16) {#each options as option}",
    ctx: ctx
  });
  return block;
} // (100:12) {#if emailTouched && !validateEmail(fields[id])}


function create_if_block_2(ctx) {
  var div;
  var t;
  var block = {
    c: function create() {
      div = element("div");
      t = text("Invalid email");
      this.h();
    },
    l: function claim(nodes) {
      div = claim_element(nodes, "DIV", {
        class: true
      });
      var div_nodes = children(div);
      t = claim_text(div_nodes, "Invalid email");
      div_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(div, "class", "message");
      add_location(div, file$1, 100, 14, 2292);
    },
    m: function mount(target, anchor) {
      insert_dev(target, div, anchor);
      append_dev(div, t);
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(div);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block_2.name,
    type: "if",
    source: "(100:12) {#if emailTouched && !validateEmail(fields[id])}",
    ctx: ctx
  });
  return block;
} // (89:6) {#each config.form.fields as { id, label, type, options, fullWidth }


function create_each_block(ctx) {
  var div;
  var label;
  var t0_value =
  /*label*/
  ctx[22] + "";
  var t0;
  var label_for_value;
  var t1;

  function select_block_type(ctx, dirty) {
    if (
    /*id*/
    ctx[21] === "email") return create_if_block_1;
    if (
    /*type*/
    ctx[23] === "text") return create_if_block_3;
    if (
    /*type*/
    ctx[23] === "number") return create_if_block_4;
    if (
    /*type*/
    ctx[23] === "select") return create_if_block_5;
    if (
    /*type*/
    ctx[23] === "textarea") return create_if_block_6;
  }

  var current_block_type = select_block_type(ctx);
  var if_block = current_block_type && current_block_type(ctx);
  var block = {
    c: function create() {
      div = element("div");
      label = element("label");
      t0 = text(t0_value);
      t1 = space();
      if (if_block) if_block.c();
      this.h();
    },
    l: function claim(nodes) {
      div = claim_element(nodes, "DIV", {
        class: true
      });
      var div_nodes = children(div);
      label = claim_element(div_nodes, "LABEL", {
        for: true
      });
      var label_nodes = children(label);
      t0 = claim_text(label_nodes, t0_value);
      label_nodes.forEach(detach_dev);
      t1 = claim_space(div_nodes);
      if (if_block) if_block.l(div_nodes);
      div_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(label, "for", label_for_value =
      /*id*/
      ctx[21]);
      add_location(label, file$1, 90, 10, 1978);
      attr_dev(div, "class", "form-control svelte-100zhid");
      toggle_class(div, "grid-full-row",
      /*fullWidth*/
      ctx[25]);
      add_location(div, file$1, 89, 8, 1909);
    },
    m: function mount(target, anchor) {
      insert_dev(target, div, anchor);
      append_dev(div, label);
      append_dev(label, t0);
      append_dev(div, t1);
      if (if_block) if_block.m(div, null);
    },
    p: function update(ctx, dirty) {
      if (dirty[0] &
      /*config*/
      1 && t0_value !== (t0_value =
      /*label*/
      ctx[22] + "")) set_data_dev(t0, t0_value);

      if (dirty[0] &
      /*config*/
      1 && label_for_value !== (label_for_value =
      /*id*/
      ctx[21])) {
        attr_dev(label, "for", label_for_value);
      }

      if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
        if_block.p(ctx, dirty);
      } else {
        if (if_block) if_block.d(1);
        if_block = current_block_type && current_block_type(ctx);

        if (if_block) {
          if_block.c();
          if_block.m(div, null);
        }
      }

      if (dirty[0] &
      /*config*/
      1) {
        toggle_class(div, "grid-full-row",
        /*fullWidth*/
        ctx[25]);
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(div);

      if (if_block) {
        if_block.d();
      }
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_each_block.name,
    type: "each",
    source: "(89:6) {#each config.form.fields as { id, label, type, options, fullWidth }",
    ctx: ctx
  });
  return block;
} // (133:8) {#if consentTouched && !fields.consent}


function create_if_block(ctx) {
  var div;
  var t;
  var block = {
    c: function create() {
      div = element("div");
      t = text("You must agree to our GDPR policy");
      this.h();
    },
    l: function claim(nodes) {
      div = claim_element(nodes, "DIV", {
        class: true
      });
      var div_nodes = children(div);
      t = claim_text(div_nodes, "You must agree to our GDPR policy");
      div_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(div, "class", "message");
      add_location(div, file$1, 133, 10, 3617);
    },
    m: function mount(target, anchor) {
      insert_dev(target, div, anchor);
      append_dev(div, t);
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(div);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block.name,
    type: "if",
    source: "(133:8) {#if consentTouched && !fields.consent}",
    ctx: ctx
  });
  return block;
} // (138:8) <Button           disabled={!formValid}           loading={pending}         >


function create_default_slot(ctx) {
  var t;
  var block = {
    c: function create() {
      t = text("Subscribe");
    },
    l: function claim(nodes) {
      t = claim_text(nodes, "Subscribe");
    },
    m: function mount(target, anchor) {
      insert_dev(target, t, anchor);
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(t);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot.name,
    type: "slot",
    source: "(138:8) <Button           disabled={!formValid}           loading={pending}         >",
    ctx: ctx
  });
  return block;
}

function create_fragment$1(ctx) {
  var form;
  var div1;
  var t0;
  var fieldset;
  var legend;
  var t1;
  var t2;
  var label;
  var input;
  var t3;
  var span0;
  var t4;
  var span1;
  var t5;
  var t6;
  var a;
  var t7;
  var t8;
  var t9;
  var div0;
  var button;
  var current;
  var mounted;
  var dispose;
  var each_value =
  /*config*/
  ctx[0].form.fields;
  validate_each_argument(each_value);
  var each_blocks = [];

  for (var i = 0; i < each_value.length; i += 1) {
    each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
  }

  var if_block =
  /*consentTouched*/
  ctx[3] && !
  /*fields*/
  ctx[6].consent && create_if_block(ctx);
  button = new Button({
    props: {
      disabled: !
      /*formValid*/
      ctx[4],
      loading:
      /*pending*/
      ctx[5],
      $$slots: {
        default: [create_default_slot]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      form = element("form");
      div1 = element("div");

      for (var _i5 = 0; _i5 < each_blocks.length; _i5 += 1) {
        each_blocks[_i5].c();
      }

      t0 = space();
      fieldset = element("fieldset");
      legend = element("legend");
      t1 = text("GDPR policy agreement*");
      t2 = space();
      label = element("label");
      input = element("input");
      t3 = space();
      span0 = element("span");
      t4 = space();
      span1 = element("span");
      t5 = text("I'm fine with");
      t6 = space();
      a = element("a");
      t7 = text("the policy terms");
      t8 = text(".\n        ");
      if (if_block) if_block.c();
      t9 = space();
      div0 = element("div");
      create_component(button.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      form = claim_element(nodes, "FORM", {
        method: true,
        action: true,
        class: true
      });
      var form_nodes = children(form);
      div1 = claim_element(form_nodes, "DIV", {
        class: true
      });
      var div1_nodes = children(div1);

      for (var _i6 = 0; _i6 < each_blocks.length; _i6 += 1) {
        each_blocks[_i6].l(div1_nodes);
      }

      t0 = claim_space(div1_nodes);
      fieldset = claim_element(div1_nodes, "FIELDSET", {
        class: true
      });
      var fieldset_nodes = children(fieldset);
      legend = claim_element(fieldset_nodes, "LEGEND", {});
      var legend_nodes = children(legend);
      t1 = claim_text(legend_nodes, "GDPR policy agreement*");
      legend_nodes.forEach(detach_dev);
      t2 = claim_space(fieldset_nodes);
      label = claim_element(fieldset_nodes, "LABEL", {
        class: true
      });
      var label_nodes = children(label);
      input = claim_element(label_nodes, "INPUT", {
        type: true,
        class: true
      });
      t3 = claim_space(label_nodes);
      span0 = claim_element(label_nodes, "SPAN", {
        class: true
      });
      children(span0).forEach(detach_dev);
      t4 = claim_space(label_nodes);
      span1 = claim_element(label_nodes, "SPAN", {
        class: true
      });
      var span1_nodes = children(span1);
      t5 = claim_text(span1_nodes, "I'm fine with");
      span1_nodes.forEach(detach_dev);
      label_nodes.forEach(detach_dev);
      t6 = claim_space(fieldset_nodes);
      a = claim_element(fieldset_nodes, "A", {
        href: true,
        target: true,
        class: true
      });
      var a_nodes = children(a);
      t7 = claim_text(a_nodes, "the policy terms");
      a_nodes.forEach(detach_dev);
      t8 = claim_text(fieldset_nodes, ".\n        ");
      if (if_block) if_block.l(fieldset_nodes);
      fieldset_nodes.forEach(detach_dev);
      t9 = claim_space(div1_nodes);
      div0 = claim_element(div1_nodes, "DIV", {
        class: true
      });
      var div0_nodes = children(div0);
      claim_component(button.$$.fragment, div0_nodes);
      div0_nodes.forEach(detach_dev);
      div1_nodes.forEach(detach_dev);
      form_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      add_location(legend, file$1, 121, 8, 3151);
      attr_dev(input, "type", "checkbox");
      attr_dev(input, "class", "svelte-100zhid");
      add_location(input, file$1, 123, 10, 3244);
      attr_dev(span0, "class", "checkbox-checkmark");
      add_location(span0, file$1, 128, 10, 3380);
      attr_dev(span1, "class", "checkbox-label");
      add_location(span1, file$1, 129, 10, 3426);
      attr_dev(label, "class", "checkbox-container");
      add_location(label, file$1, 122, 8, 3199);
      attr_dev(a, "href", "/consent");
      attr_dev(a, "target", "_blank");
      attr_dev(a, "class", "svelte-100zhid");
      add_location(a, file$1, 131, 8, 3502);
      attr_dev(fieldset, "class", "form-control grid-full-row svelte-100zhid");
      add_location(fieldset, file$1, 120, 6, 3097);
      attr_dev(div0, "class", "form-control grid-send-button svelte-100zhid");
      add_location(div0, file$1, 136, 6, 3716);
      attr_dev(div1, "class", "form-wrapper svelte-100zhid");
      add_location(div1, file$1, 87, 4, 1795);
      attr_dev(form, "method", "post");
      attr_dev(form, "action", "/leads");
      attr_dev(form, "class", "svelte-100zhid");
      add_location(form, file$1, 81, 2, 1678);
    },
    m: function mount(target, anchor) {
      insert_dev(target, form, anchor);
      append_dev(form, div1);

      for (var _i7 = 0; _i7 < each_blocks.length; _i7 += 1) {
        each_blocks[_i7].m(div1, null);
      }

      append_dev(div1, t0);
      append_dev(div1, fieldset);
      append_dev(fieldset, legend);
      append_dev(legend, t1);
      append_dev(fieldset, t2);
      append_dev(fieldset, label);
      append_dev(label, input);
      input.checked =
      /*fields*/
      ctx[6].consent;
      append_dev(label, t3);
      append_dev(label, span0);
      append_dev(label, t4);
      append_dev(label, span1);
      append_dev(span1, t5);
      append_dev(fieldset, t6);
      append_dev(fieldset, a);
      append_dev(a, t7);
      append_dev(fieldset, t8);
      if (if_block) if_block.m(fieldset, null);
      append_dev(div1, t9);
      append_dev(div1, div0);
      mount_component(button, div0, null);
      /*form_binding*/

      ctx[16](form);
      current = true;

      if (!mounted) {
        dispose = [listen_dev(input, "click",
        /*touchConsent*/
        ctx[8], false, false, false), listen_dev(input, "change",
        /*input_change_handler*/
        ctx[15]), listen_dev(form, "submit", prevent_default(
        /*onSubmit*/
        ctx[9]), false, true, false)];
        mounted = true;
      }
    },
    p: function update(ctx, dirty) {
      if (dirty[0] &
      /*config, emailTouched, fields, touchEmail*/
      197) {
        each_value =
        /*config*/
        ctx[0].form.fields;
        validate_each_argument(each_value);

        var _i8;

        for (_i8 = 0; _i8 < each_value.length; _i8 += 1) {
          var child_ctx = get_each_context(ctx, each_value, _i8);

          if (each_blocks[_i8]) {
            each_blocks[_i8].p(child_ctx, dirty);
          } else {
            each_blocks[_i8] = create_each_block(child_ctx);

            each_blocks[_i8].c();

            each_blocks[_i8].m(div1, t0);
          }
        }

        for (; _i8 < each_blocks.length; _i8 += 1) {
          each_blocks[_i8].d(1);
        }

        each_blocks.length = each_value.length;
      }

      if (dirty[0] &
      /*fields, config*/
      65) {
        input.checked =
        /*fields*/
        ctx[6].consent;
      }

      if (
      /*consentTouched*/
      ctx[3] && !
      /*fields*/
      ctx[6].consent) {
        if (if_block) ; else {
          if_block = create_if_block(ctx);
          if_block.c();
          if_block.m(fieldset, null);
        }
      } else if (if_block) {
        if_block.d(1);
        if_block = null;
      }

      var button_changes = {};
      if (dirty[0] &
      /*formValid*/
      16) button_changes.disabled = !
      /*formValid*/
      ctx[4];
      if (dirty[0] &
      /*pending*/
      32) button_changes.loading =
      /*pending*/
      ctx[5];

      if (dirty[1] &
      /*$$scope*/
      1) {
        button_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      button.$set(button_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(button.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(button.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(form);
      destroy_each(each_blocks, detaching);
      if (if_block) if_block.d();
      destroy_component(button);
      /*form_binding*/

      ctx[16](null);
      mounted = false;
      run_all(dispose);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment$1.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function instance$1($$self, $$props, $$invalidate) {
  var dispatch = createEventDispatcher();
  var _$$props$config = $$props.config,
      config = _$$props$config === void 0 ? {} : _$$props$config;
  var STATE = {
    SUCCESS: "SUCCESS",
    ERROR: "ERROR",
    IDLE: "IDLE"
  };
  var leadForm;
  var emailTouched;
  var consentTouched;
  var formValid;
  var state = STATE.IDLE;
  var pending = false;
  var fields = {
    consent: null,
    context: config.headline
  };

  var getFieldsFromConfig = function getFieldsFromConfig() {
    return config.form.fields.reduce(function (acc, val) {
      return _objectSpread$1(_objectSpread$1({}, acc), {}, _defineProperty({}, val.id, null));
    }, {});
  };

  fields = _objectSpread$1(_objectSpread$1({}, fields), getFieldsFromConfig());

  var touchEmail = function touchEmail() {
    $$invalidate(2, emailTouched = true);
  };

  var touchConsent = function touchConsent() {
    $$invalidate(3, consentTouched = true);
  };

  var onSubmit = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regenerator.mark(function _callee(event) {
      var formData, data, _iterator, _step, _step$value, key, value, response;

      return regenerator.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              $$invalidate(5, pending = true);
              formData = new FormData(event.target);
              data = {};
              _iterator = _createForOfIteratorHelper(formData.entries());

              try {
                for (_iterator.s(); !(_step = _iterator.n()).done;) {
                  _step$value = _slicedToArray(_step.value, 2), key = _step$value[0], value = _step$value[1];
                  data[key] = value;
                }
              } catch (err) {
                _iterator.e(err);
              } finally {
                _iterator.f();
              }

              _context.prev = 5;
              _context.next = 8;
              return post(event.target.action, data);

            case 8:
              response = _context.sent;
              $$invalidate(17, state = response.type === "success" ? STATE.SUCCESS : STATE.ERROR);
              $$invalidate(5, pending = false);
              _context.next = 18;
              break;

            case 13:
              _context.prev = 13;
              _context.t0 = _context["catch"](5);
              console.error(_context.t0);
              $$invalidate(17, state = STATE.ERROR);
              $$invalidate(5, pending = false);

            case 18:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[5, 13]]);
    }));

    return function onSubmit(_x) {
      return _ref.apply(this, arguments);
    };
  }();

  var writable_props = ["config"];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console_1.warn("<LeadForm> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("LeadForm", $$slots, []);

  function input_input_handler(id) {
    fields[id] = this.value;
    $$invalidate(6, fields);
    $$invalidate(0, config);
  }

  function input_input_handler_1(id) {
    fields[id] = this.value;
    $$invalidate(6, fields);
    $$invalidate(0, config);
  }

  function input_input_handler_2(id) {
    fields[id] = to_number(this.value);
    $$invalidate(6, fields);
    $$invalidate(0, config);
  }

  function select_change_handler(id) {
    fields[id] = select_value(this);
    $$invalidate(6, fields);
    $$invalidate(0, config);
  }

  function textarea_input_handler(id) {
    fields[id] = this.value;
    $$invalidate(6, fields);
    $$invalidate(0, config);
  }

  function input_change_handler() {
    fields.consent = this.checked;
    $$invalidate(6, fields);
    $$invalidate(0, config);
  }

  function form_binding($$value) {
    binding_callbacks[$$value ? "unshift" : "push"](function () {
      leadForm = $$value;
      $$invalidate(1, leadForm);
    });
  }

  $$self.$set = function ($$props) {
    if ("config" in $$props) $$invalidate(0, config = $$props.config);
  };

  $$self.$capture_state = function () {
    return {
      Icon: Icon,
      Button: Button,
      createEventDispatcher: createEventDispatcher,
      validateEmail: email,
      post: post,
      expoIn: expoIn,
      expoOut: expoOut,
      dispatch: dispatch,
      config: config,
      STATE: STATE,
      leadForm: leadForm,
      emailTouched: emailTouched,
      consentTouched: consentTouched,
      formValid: formValid,
      state: state,
      pending: pending,
      fields: fields,
      getFieldsFromConfig: getFieldsFromConfig,
      touchEmail: touchEmail,
      touchConsent: touchConsent,
      onSubmit: onSubmit
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("config" in $$props) $$invalidate(0, config = $$props.config);
    if ("leadForm" in $$props) $$invalidate(1, leadForm = $$props.leadForm);
    if ("emailTouched" in $$props) $$invalidate(2, emailTouched = $$props.emailTouched);
    if ("consentTouched" in $$props) $$invalidate(3, consentTouched = $$props.consentTouched);
    if ("formValid" in $$props) $$invalidate(4, formValid = $$props.formValid);
    if ("state" in $$props) $$invalidate(17, state = $$props.state);
    if ("pending" in $$props) $$invalidate(5, pending = $$props.pending);
    if ("fields" in $$props) $$invalidate(6, fields = $$props.fields);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  $$self.$$.update = function () {
    if ($$self.$$.dirty[0] &
    /*fields*/
    64) {
       {
        $$invalidate(4, formValid = email(fields.email) && fields.consent === true);
      }
    }

    if ($$self.$$.dirty[0] &
    /*state, leadForm*/
    131074) {
       {
        dispatch("change", state);

        if (state === STATE.SUCCESS) {
          leadForm.reset();
        }
      }
    }
  };

  return [config, leadForm, emailTouched, consentTouched, formValid, pending, fields, touchEmail, touchConsent, onSubmit, input_input_handler, input_input_handler_1, input_input_handler_2, select_change_handler, textarea_input_handler, input_change_handler, form_binding];
}

var LeadForm = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(LeadForm, _SvelteComponentDev);

  var _super = _createSuper$1(LeadForm);

  function LeadForm(options) {
    var _this;

    _classCallCheck(this, LeadForm);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance$1, create_fragment$1, safe_not_equal, {
      config: 0
    }, [-1, -1]);
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "LeadForm",
      options: options,
      id: create_fragment$1.name
    });
    return _this;
  }

  _createClass(LeadForm, [{
    key: "config",
    get: function get() {
      throw new Error("<LeadForm>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<LeadForm>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }]);

  return LeadForm;
}(SvelteComponentDev);

function _createSuper$2(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$2(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct$2() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
var file$2 = "src/routes/contact.svelte"; // (80:0) <Container>

function create_default_slot_2(ctx) {
  var h1;
  var t0;
  var t1;
  var ul;
  var li0;
  var a0;
  var icon0;
  var t2;
  var li1;
  var a1;
  var icon1;
  var t3;
  var li2;
  var a2;
  var icon2;
  var t4;
  var address0;
  var t5;
  var br0;
  var t6;
  var a3;
  var t7;
  var br1;
  var t8;
  var br2;
  var t9;
  var a4;
  var t10;
  var t11;
  var address1;
  var t12;
  var br3;
  var t13;
  var br4;
  var t14;
  var a5;
  var t15;
  var t16;
  var section;
  var h2;
  var t17;
  var t18;
  var p;
  var t19;
  var t20;
  var leadform;
  var t21;
  var pagecta;
  var current;
  icon0 = new Icon({
    props: {
      id: "FACEBOOK",
      width: 50,
      height: 50
    },
    $$inline: true
  });
  icon1 = new Icon({
    props: {
      id: "INSTAGRAM",
      width: 50,
      height: 50
    },
    $$inline: true
  });
  icon2 = new Icon({
    props: {
      id: "LINKEDIN",
      width: 50,
      height: 50
    },
    $$inline: true
  });
  leadform = new LeadForm({
    props: {
      config:
      /*leadFormConfig*/
      ctx[3]
    },
    $$inline: true
  });
  leadform.$on("change",
  /*onFormStateChange*/
  ctx[5]);
  pagecta = new PageCTA({
    props: {
      links: [{
        link: "about",
        text: "About Wayout"
      }, {
        link: "media",
        text: "Media & Press"
      }]
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      h1 = element("h1");
      t0 = text(
      /*title*/
      ctx[4]);
      t1 = space();
      ul = element("ul");
      li0 = element("li");
      a0 = element("a");
      create_component(icon0.$$.fragment);
      t2 = space();
      li1 = element("li");
      a1 = element("a");
      create_component(icon1.$$.fragment);
      t3 = space();
      li2 = element("li");
      a2 = element("a");
      create_component(icon2.$$.fragment);
      t4 = space();
      address0 = element("address");
      t5 = text("Wayout International AB");
      br0 = element("br");
      t6 = space();
      a3 = element("a");
      t7 = text("contact@wayoutintl.com");
      br1 = element("br");
      t8 = text("\n    Nybrogatan 8, 114 34 Stockholm, Sweden");
      br2 = element("br");
      t9 = space();
      a4 = element("a");
      t10 = text("+46 708 10 27 99");
      t11 = space();
      address1 = element("address");
      t12 = text("PR/Press contact: ");
      br3 = element("br");
      t13 = text("\n    Carolina Gahn, Gahn Fishing ");
      br4 = element("br");
      t14 = space();
      a5 = element("a");
      t15 = text("carolina@gahnfishing.com");
      t16 = space();
      section = element("section");
      h2 = element("h2");
      t17 = text("Wayout Newsletter");
      t18 = space();
      p = element("p");
      t19 = text("Subscribe to Wayout news.");
      t20 = space();
      create_component(leadform.$$.fragment);
      t21 = space();
      create_component(pagecta.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      h1 = claim_element(nodes, "H1", {
        class: true
      });
      var h1_nodes = children(h1);
      t0 = claim_text(h1_nodes,
      /*title*/
      ctx[4]);
      h1_nodes.forEach(detach_dev);
      t1 = claim_space(nodes);
      ul = claim_element(nodes, "UL", {
        class: true
      });
      var ul_nodes = children(ul);
      li0 = claim_element(ul_nodes, "LI", {});
      var li0_nodes = children(li0);
      a0 = claim_element(li0_nodes, "A", {
        href: true,
        target: true,
        class: true
      });
      var a0_nodes = children(a0);
      claim_component(icon0.$$.fragment, a0_nodes);
      a0_nodes.forEach(detach_dev);
      li0_nodes.forEach(detach_dev);
      t2 = claim_space(ul_nodes);
      li1 = claim_element(ul_nodes, "LI", {});
      var li1_nodes = children(li1);
      a1 = claim_element(li1_nodes, "A", {
        href: true,
        target: true,
        class: true
      });
      var a1_nodes = children(a1);
      claim_component(icon1.$$.fragment, a1_nodes);
      a1_nodes.forEach(detach_dev);
      li1_nodes.forEach(detach_dev);
      t3 = claim_space(ul_nodes);
      li2 = claim_element(ul_nodes, "LI", {});
      var li2_nodes = children(li2);
      a2 = claim_element(li2_nodes, "A", {
        href: true,
        target: true,
        class: true
      });
      var a2_nodes = children(a2);
      claim_component(icon2.$$.fragment, a2_nodes);
      a2_nodes.forEach(detach_dev);
      li2_nodes.forEach(detach_dev);
      ul_nodes.forEach(detach_dev);
      t4 = claim_space(nodes);
      address0 = claim_element(nodes, "ADDRESS", {
        class: true
      });
      var address0_nodes = children(address0);
      t5 = claim_text(address0_nodes, "Wayout International AB");
      br0 = claim_element(address0_nodes, "BR", {});
      t6 = claim_space(address0_nodes);
      a3 = claim_element(address0_nodes, "A", {
        href: true,
        class: true
      });
      var a3_nodes = children(a3);
      t7 = claim_text(a3_nodes, "contact@wayoutintl.com");
      a3_nodes.forEach(detach_dev);
      br1 = claim_element(address0_nodes, "BR", {});
      t8 = claim_text(address0_nodes, "\n    Nybrogatan 8, 114 34 Stockholm, Sweden");
      br2 = claim_element(address0_nodes, "BR", {});
      t9 = claim_space(address0_nodes);
      a4 = claim_element(address0_nodes, "A", {
        href: true,
        class: true
      });
      var a4_nodes = children(a4);
      t10 = claim_text(a4_nodes, "+46 708 10 27 99");
      a4_nodes.forEach(detach_dev);
      address0_nodes.forEach(detach_dev);
      t11 = claim_space(nodes);
      address1 = claim_element(nodes, "ADDRESS", {
        class: true
      });
      var address1_nodes = children(address1);
      t12 = claim_text(address1_nodes, "PR/Press contact: ");
      br3 = claim_element(address1_nodes, "BR", {});
      t13 = claim_text(address1_nodes, "\n    Carolina Gahn, Gahn Fishing ");
      br4 = claim_element(address1_nodes, "BR", {});
      t14 = claim_space(address1_nodes);
      a5 = claim_element(address1_nodes, "A", {
        href: true,
        class: true
      });
      var a5_nodes = children(a5);
      t15 = claim_text(a5_nodes, "carolina@gahnfishing.com");
      a5_nodes.forEach(detach_dev);
      address1_nodes.forEach(detach_dev);
      t16 = claim_space(nodes);
      section = claim_element(nodes, "SECTION", {});
      var section_nodes = children(section);
      h2 = claim_element(section_nodes, "H2", {});
      var h2_nodes = children(h2);
      t17 = claim_text(h2_nodes, "Wayout Newsletter");
      h2_nodes.forEach(detach_dev);
      t18 = claim_space(section_nodes);
      p = claim_element(section_nodes, "P", {});
      var p_nodes = children(p);
      t19 = claim_text(p_nodes, "Subscribe to Wayout news.");
      p_nodes.forEach(detach_dev);
      t20 = claim_space(section_nodes);
      claim_component(leadform.$$.fragment, section_nodes);
      section_nodes.forEach(detach_dev);
      t21 = claim_space(nodes);
      claim_component(pagecta.$$.fragment, nodes);
      this.h();
    },
    h: function hydrate() {
      attr_dev(h1, "class", "svelte-1wuur43");
      add_location(h1, file$2, 80, 2, 2144);
      attr_dev(a0, "href", "https://www.facebook.com/wayoutintl/");
      attr_dev(a0, "target", "_blank");
      attr_dev(a0, "class", "svelte-1wuur43");
      add_location(a0, file$2, 84, 6, 2205);
      add_location(li0, file$2, 83, 4, 2194);
      attr_dev(a1, "href", "https://www.instagram.com/wayoutintl/");
      attr_dev(a1, "target", "_blank");
      attr_dev(a1, "class", "svelte-1wuur43");
      add_location(a1, file$2, 87, 6, 2343);
      add_location(li1, file$2, 86, 4, 2332);
      attr_dev(a2, "href", "https://www.linkedin.com/company/wayout-intl-ab/");
      attr_dev(a2, "target", "_blank");
      attr_dev(a2, "class", "svelte-1wuur43");
      add_location(a2, file$2, 90, 6, 2483);
      add_location(li2, file$2, 89, 4, 2472);
      attr_dev(ul, "class", "social-media svelte-1wuur43");
      add_location(ul, file$2, 82, 2, 2164);
      add_location(br0, file$2, 94, 27, 2665);
      attr_dev(a3, "href", "mailto:contact@wayoutint.com");
      attr_dev(a3, "class", "svelte-1wuur43");
      add_location(a3, file$2, 95, 4, 2674);
      add_location(br1, file$2, 95, 69, 2739);
      add_location(br2, file$2, 96, 42, 2786);
      attr_dev(a4, "href", "tel:+46708102799");
      attr_dev(a4, "class", "svelte-1wuur43");
      add_location(a4, file$2, 97, 4, 2795);
      attr_dev(address0, "class", "svelte-1wuur43");
      add_location(address0, file$2, 93, 2, 2628);
      add_location(br3, file$2, 101, 22, 2891);
      add_location(br4, file$2, 102, 32, 2928);
      attr_dev(a5, "href", "mailto:carolina@gahnfishing.com");
      attr_dev(a5, "class", "svelte-1wuur43");
      add_location(a5, file$2, 103, 4, 2937);
      attr_dev(address1, "class", "svelte-1wuur43");
      add_location(address1, file$2, 100, 2, 2859);
      add_location(h2, file$2, 107, 4, 3038);
      add_location(p, file$2, 108, 4, 3069);
      add_location(section, file$2, 106, 2, 3024);
    },
    m: function mount(target, anchor) {
      insert_dev(target, h1, anchor);
      append_dev(h1, t0);
      insert_dev(target, t1, anchor);
      insert_dev(target, ul, anchor);
      append_dev(ul, li0);
      append_dev(li0, a0);
      mount_component(icon0, a0, null);
      append_dev(ul, t2);
      append_dev(ul, li1);
      append_dev(li1, a1);
      mount_component(icon1, a1, null);
      append_dev(ul, t3);
      append_dev(ul, li2);
      append_dev(li2, a2);
      mount_component(icon2, a2, null);
      insert_dev(target, t4, anchor);
      insert_dev(target, address0, anchor);
      append_dev(address0, t5);
      append_dev(address0, br0);
      append_dev(address0, t6);
      append_dev(address0, a3);
      append_dev(a3, t7);
      append_dev(address0, br1);
      append_dev(address0, t8);
      append_dev(address0, br2);
      append_dev(address0, t9);
      append_dev(address0, a4);
      append_dev(a4, t10);
      insert_dev(target, t11, anchor);
      insert_dev(target, address1, anchor);
      append_dev(address1, t12);
      append_dev(address1, br3);
      append_dev(address1, t13);
      append_dev(address1, br4);
      append_dev(address1, t14);
      append_dev(address1, a5);
      append_dev(a5, t15);
      insert_dev(target, t16, anchor);
      insert_dev(target, section, anchor);
      append_dev(section, h2);
      append_dev(h2, t17);
      append_dev(section, t18);
      append_dev(section, p);
      append_dev(p, t19);
      append_dev(section, t20);
      mount_component(leadform, section, null);
      insert_dev(target, t21, anchor);
      mount_component(pagecta, target, anchor);
      current = true;
    },
    p: noop,
    i: function intro(local) {
      if (current) return;
      transition_in(icon0.$$.fragment, local);
      transition_in(icon1.$$.fragment, local);
      transition_in(icon2.$$.fragment, local);
      transition_in(leadform.$$.fragment, local);
      transition_in(pagecta.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(icon0.$$.fragment, local);
      transition_out(icon1.$$.fragment, local);
      transition_out(icon2.$$.fragment, local);
      transition_out(leadform.$$.fragment, local);
      transition_out(pagecta.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(h1);
      if (detaching) detach_dev(t1);
      if (detaching) detach_dev(ul);
      destroy_component(icon0);
      destroy_component(icon1);
      destroy_component(icon2);
      if (detaching) detach_dev(t4);
      if (detaching) detach_dev(address0);
      if (detaching) detach_dev(t11);
      if (detaching) detach_dev(address1);
      if (detaching) detach_dev(t16);
      if (detaching) detach_dev(section);
      destroy_component(leadform);
      if (detaching) detach_dev(t21);
      destroy_component(pagecta, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot_2.name,
    type: "slot",
    source: "(80:0) <Container>",
    ctx: ctx
  });
  return block;
} // (114:0) {#if $modalVisible}


function create_if_block$1(ctx) {
  var modal;
  var current;
  modal = new Modal({
    props: {
      $$slots: {
        default: [create_default_slot$1]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      create_component(modal.$$.fragment);
    },
    l: function claim(nodes) {
      claim_component(modal.$$.fragment, nodes);
    },
    m: function mount(target, anchor) {
      mount_component(modal, target, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      var modal_changes = {};

      if (dirty &
      /*$$scope, modalMessage, modalTitle*/
      131) {
        modal_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      modal.$set(modal_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(modal.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(modal.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      destroy_component(modal, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block$1.name,
    type: "if",
    source: "(114:0) {#if $modalVisible}",
    ctx: ctx
  });
  return block;
} // (124:6) <Button icon="CHECK" on:click={() => modalVisible.set(false)}>


function create_default_slot_1(ctx) {
  var t;
  var block = {
    c: function create() {
      t = text("Ok!");
    },
    l: function claim(nodes) {
      t = claim_text(nodes, "Ok!");
    },
    m: function mount(target, anchor) {
      insert_dev(target, t, anchor);
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(t);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot_1.name,
    type: "slot",
    source: "(124:6) <Button icon=\\\"CHECK\\\" on:click={() => modalVisible.set(false)}>",
    ctx: ctx
  });
  return block;
} // (115:2) <Modal>


function create_default_slot$1(ctx) {
  var div;
  var h2;
  var t0;
  var p;
  var t1;
  var button;
  var current;
  button = new Button({
    props: {
      icon: "CHECK",
      $$slots: {
        default: [create_default_slot_1]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  button.$on("click",
  /*click_handler*/
  ctx[6]);
  var block = {
    c: function create() {
      div = element("div");
      h2 = element("h2");
      t0 = space();
      p = element("p");
      t1 = space();
      create_component(button.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      div = claim_element(nodes, "DIV", {
        class: true
      });
      var div_nodes = children(div);
      h2 = claim_element(div_nodes, "H2", {
        class: true
      });
      var h2_nodes = children(h2);
      h2_nodes.forEach(detach_dev);
      t0 = claim_space(div_nodes);
      p = claim_element(div_nodes, "P", {
        class: true
      });
      var p_nodes = children(p);
      p_nodes.forEach(detach_dev);
      t1 = claim_space(div_nodes);
      claim_component(button.$$.fragment, div_nodes);
      div_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(h2, "class", "svelte-1wuur43");
      add_location(h2, file$2, 117, 6, 3362);
      attr_dev(p, "class", "svelte-1wuur43");
      add_location(p, file$2, 120, 6, 3412);
      attr_dev(div, "class", "wrapper svelte-1wuur43");
      add_location(div, file$2, 115, 4, 3333);
    },
    m: function mount(target, anchor) {
      insert_dev(target, div, anchor);
      append_dev(div, h2);
      h2.innerHTML =
      /*modalTitle*/
      ctx[0];
      append_dev(div, t0);
      append_dev(div, p);
      p.innerHTML =
      /*modalMessage*/
      ctx[1];
      append_dev(div, t1);
      mount_component(button, div, null);
      current = true;
    },
    p: function update(ctx, dirty) {
      if (!current || dirty &
      /*modalTitle*/
      1) h2.innerHTML =
      /*modalTitle*/
      ctx[0];
      if (!current || dirty &
      /*modalMessage*/
      2) p.innerHTML =
      /*modalMessage*/
      ctx[1];
      var button_changes = {};

      if (dirty &
      /*$$scope*/
      128) {
        button_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      button.$set(button_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(button.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(button.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(div);
      destroy_component(button);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot$1.name,
    type: "slot",
    source: "(115:2) <Modal>",
    ctx: ctx
  });
  return block;
}

function create_fragment$2(ctx) {
  var title_value;
  var t0;
  var container;
  var t1;
  var if_block_anchor;
  var current;
  document.title = title_value = "Wayout - " +
  /*title*/
  ctx[4];
  container = new Container({
    props: {
      $$slots: {
        default: [create_default_slot_2]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var if_block =
  /*$modalVisible*/
  ctx[2] && create_if_block$1(ctx);
  var block = {
    c: function create() {
      t0 = space();
      create_component(container.$$.fragment);
      t1 = space();
      if (if_block) if_block.c();
      if_block_anchor = empty();
    },
    l: function claim(nodes) {
      var head_nodes = query_selector_all("[data-svelte=\"svelte-1ptlpyn\"]", document.head);
      head_nodes.forEach(detach_dev);
      t0 = claim_space(nodes);
      claim_component(container.$$.fragment, nodes);
      t1 = claim_space(nodes);
      if (if_block) if_block.l(nodes);
      if_block_anchor = empty();
    },
    m: function mount(target, anchor) {
      insert_dev(target, t0, anchor);
      mount_component(container, target, anchor);
      insert_dev(target, t1, anchor);
      if (if_block) if_block.m(target, anchor);
      insert_dev(target, if_block_anchor, anchor);
      current = true;
    },
    p: function update(ctx, _ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];

      if ((!current || dirty &
      /*title*/
      16) && title_value !== (title_value = "Wayout - " +
      /*title*/
      ctx[4])) {
        document.title = title_value;
      }

      var container_changes = {};

      if (dirty &
      /*$$scope*/
      128) {
        container_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      container.$set(container_changes);

      if (
      /*$modalVisible*/
      ctx[2]) {
        if (if_block) {
          if_block.p(ctx, dirty);

          if (dirty &
          /*$modalVisible*/
          4) {
            transition_in(if_block, 1);
          }
        } else {
          if_block = create_if_block$1(ctx);
          if_block.c();
          transition_in(if_block, 1);
          if_block.m(if_block_anchor.parentNode, if_block_anchor);
        }
      } else if (if_block) {
        group_outros();
        transition_out(if_block, 1, 1, function () {
          if_block = null;
        });
        check_outros();
      }
    },
    i: function intro(local) {
      if (current) return;
      transition_in(container.$$.fragment, local);
      transition_in(if_block);
      current = true;
    },
    o: function outro(local) {
      transition_out(container.$$.fragment, local);
      transition_out(if_block);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(t0);
      destroy_component(container, detaching);
      if (detaching) detach_dev(t1);
      if (if_block) if_block.d(detaching);
      if (detaching) detach_dev(if_block_anchor);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment$2.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function instance$2($$self, $$props, $$invalidate) {
  var $modalVisible;
  validate_store(modalVisible, "modalVisible");
  component_subscribe($$self, modalVisible, function ($$value) {
    return $$invalidate(2, $modalVisible = $$value);
  });
  var leadFormConfig = {
    headline: "Keep me updated on local availability",
    text: "Kindly provide some initial info and we’ll keep you in the loop!",
    form: {
      fields: [//        {
      //          id: "inquerytype",
      //          label: "Inquery type",
      //          type: "select",
      //          options: ["Get updates on availability", "Something else"],
      //          fullWidth: true,
      //        },
      //        {
      //          id: "householdsize",
      //          label: "Household size",
      //          type: "select",
      //          options: ["1", "2", "3", "4", "5", "other"],
      //        },
      //        {
      //          id: "interest",
      //          label: "Interest in",
      //          type: "select",
      //          options: [
      //            "household water",
      //            "workplace water",
      //            "community water",
      //            "other",
      //          ],
      //        },
      {
        id: "email",
        label: "Email*",
        type: "text",
        fullWidth: true
      }] //        {
      //          id: "location",

    } //          label: "Location",
    //          type: "text",

  }; //        },
  //        {
  //          id: "message",
  //          label: "Message",
  //          type: "textarea",
  //          fullWidth: true,
  //        },

  var modalTitle;
  var modalMessage;
  var title = "Contact";

  var onFormStateChange = function onFormStateChange(event) {
    if (event.detail === "SUCCESS") {
      $$invalidate(0, modalTitle = "Thank you!");
      $$invalidate(1, modalMessage = "Message recieved!");
      modalVisible.set(true);
    } else if (event.detail === "ERROR") {
      $$invalidate(0, modalTitle = "Something <br> went wrong...");
      $$invalidate(1, modalMessage = "Please, try again later.");
      modalVisible.set(true);
    }
  };

  var writable_props = [];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn("<Contact> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("Contact", $$slots, []);

  var click_handler = function click_handler() {
    return modalVisible.set(false);
  };

  $$self.$capture_state = function () {
    return {
      Modal: Modal,
      LeadForm: LeadForm,
      modalVisible: modalVisible,
      Icon: Icon,
      Button: Button,
      Container: Container,
      PageCTA: PageCTA,
      leadFormConfig: leadFormConfig,
      modalTitle: modalTitle,
      modalMessage: modalMessage,
      title: title,
      onFormStateChange: onFormStateChange,
      $modalVisible: $modalVisible
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("modalTitle" in $$props) $$invalidate(0, modalTitle = $$props.modalTitle);
    if ("modalMessage" in $$props) $$invalidate(1, modalMessage = $$props.modalMessage);
    if ("title" in $$props) $$invalidate(4, title = $$props.title);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  return [modalTitle, modalMessage, $modalVisible, leadFormConfig, title, onFormStateChange, click_handler];
}

var Contact = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(Contact, _SvelteComponentDev);

  var _super = _createSuper$2(Contact);

  function Contact(options) {
    var _this;

    _classCallCheck(this, Contact);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance$2, create_fragment$2, safe_not_equal, {});
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "Contact",
      options: options,
      id: create_fragment$2.name
    });
    return _this;
  }

  return Contact;
}(SvelteComponentDev);

export default Contact;
