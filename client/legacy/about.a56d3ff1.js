import { a as _asyncToGenerator, r as regenerator, c as _inherits, d as _getPrototypeOf, e as _possibleConstructorReturn, f as _classCallCheck, i as init, s as safe_not_equal, g as _assertThisInitialized, h as dispatch_dev, j as _createClass, S as SvelteComponentDev, v as validate_slots, K as Icon, m as space, M as create_component, ad as query_selector_all, q as detach_dev, t as claim_space, O as claim_component, z as insert_dev, P as mount_component, b as _slicedToArray, R as transition_in, T as transition_out, U as destroy_component, l as element, n as claim_element, u as attr_dev, x as add_location, L as text, p as children, N as claim_text, ao as HtmlTag, A as append_dev, Q as set_data_dev } from './client.dc955f77.js';
import { _ as _defineProperty } from './defineProperty.bdf5641a.js';
import { C as Container } from './Container.19136de0.js';
import { B as Button, P as PageCTA } from './PageCTA.ddb524e9.js';

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
var file = "src/routes/about.svelte"; // (31:4) {#if image}

function create_if_block(ctx) {
  var img;
  var img_src_value;
  var block = {
    c: function create() {
      img = element("img");
      this.h();
    },
    l: function claim(nodes) {
      img = claim_element(nodes, "IMG", {
        src: true,
        alt: true,
        class: true
      });
      this.h();
    },
    h: function hydrate() {
      if (img.src !== (img_src_value =
      /*image*/
      ctx[2])) attr_dev(img, "src", img_src_value);
      attr_dev(img, "alt",
      /*title*/
      ctx[0]);
      attr_dev(img, "class", "svelte-p9fowv");
      add_location(img, file, 31, 6, 693);
    },
    m: function mount(target, anchor) {
      insert_dev(target, img, anchor);
    },
    p: function update(ctx, dirty) {
      if (dirty &
      /*image*/
      4 && img.src !== (img_src_value =
      /*image*/
      ctx[2])) {
        attr_dev(img, "src", img_src_value);
      }

      if (dirty &
      /*title*/
      1) {
        attr_dev(img, "alt",
        /*title*/
        ctx[0]);
      }
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(img);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block.name,
    type: "if",
    source: "(31:4) {#if image}",
    ctx: ctx
  });
  return block;
} // (28:0) <Container>


function create_default_slot(ctx) {
  var article;
  var h1;
  var t0;
  var t1;
  var t2;
  var html_tag;
  var t3;
  var pagecta;
  var current;
  var if_block =
  /*image*/
  ctx[2] && create_if_block(ctx);
  pagecta = new PageCTA({
    props: {
      links: [{
        link: "contact",
        text: "Contact"
      }, {
        link: "media",
        text: "Press & Media"
      }]
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      article = element("article");
      h1 = element("h1");
      t0 = text(
      /*title*/
      ctx[0]);
      t1 = space();
      if (if_block) if_block.c();
      t2 = space();
      t3 = space();
      create_component(pagecta.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      article = claim_element(nodes, "ARTICLE", {
        class: true
      });
      var article_nodes = children(article);
      h1 = claim_element(article_nodes, "H1", {
        class: true
      });
      var h1_nodes = children(h1);
      t0 = claim_text(h1_nodes,
      /*title*/
      ctx[0]);
      h1_nodes.forEach(detach_dev);
      t1 = claim_space(article_nodes);
      if (if_block) if_block.l(article_nodes);
      t2 = claim_space(article_nodes);
      article_nodes.forEach(detach_dev);
      t3 = claim_space(nodes);
      claim_component(pagecta.$$.fragment, nodes);
      this.h();
    },
    h: function hydrate() {
      attr_dev(h1, "class", "svelte-p9fowv");
      add_location(h1, file, 29, 4, 654);
      html_tag = new HtmlTag(null);
      attr_dev(article, "class", "discrete");
      add_location(article, file, 28, 2, 623);
    },
    m: function mount(target, anchor) {
      insert_dev(target, article, anchor);
      append_dev(article, h1);
      append_dev(h1, t0);
      append_dev(article, t1);
      if (if_block) if_block.m(article, null);
      append_dev(article, t2);
      html_tag.m(
      /*body*/
      ctx[1], article);
      insert_dev(target, t3, anchor);
      mount_component(pagecta, target, anchor);
      current = true;
    },
    p: function update(ctx, dirty) {
      if (!current || dirty &
      /*title*/
      1) set_data_dev(t0,
      /*title*/
      ctx[0]);

      if (
      /*image*/
      ctx[2]) {
        if (if_block) {
          if_block.p(ctx, dirty);
        } else {
          if_block = create_if_block(ctx);
          if_block.c();
          if_block.m(article, t2);
        }
      } else if (if_block) {
        if_block.d(1);
        if_block = null;
      }

      if (!current || dirty &
      /*body*/
      2) html_tag.p(
      /*body*/
      ctx[1]);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(pagecta.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(pagecta.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(article);
      if (if_block) if_block.d();
      if (detaching) detach_dev(t3);
      destroy_component(pagecta, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot.name,
    type: "slot",
    source: "(28:0) <Container>",
    ctx: ctx
  });
  return block;
}

function create_fragment(ctx) {
  var title_value;
  var t;
  var container;
  var current;
  document.title = title_value = "Wayout - " +
  /*title*/
  ctx[0];
  container = new Container({
    props: {
      $$slots: {
        default: [create_default_slot]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      t = space();
      create_component(container.$$.fragment);
    },
    l: function claim(nodes) {
      var head_nodes = query_selector_all("[data-svelte=\"svelte-1ptlpyn\"]", document.head);
      head_nodes.forEach(detach_dev);
      t = claim_space(nodes);
      claim_component(container.$$.fragment, nodes);
    },
    m: function mount(target, anchor) {
      insert_dev(target, t, anchor);
      mount_component(container, target, anchor);
      current = true;
    },
    p: function update(ctx, _ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];

      if ((!current || dirty &
      /*title*/
      1) && title_value !== (title_value = "Wayout - " +
      /*title*/
      ctx[0])) {
        document.title = title_value;
      }

      var container_changes = {};

      if (dirty &
      /*$$scope, body, image, title*/
      15) {
        container_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      container.$set(container_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(container.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(container.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(t);
      destroy_component(container, detaching);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function preload() {
  return _preload.apply(this, arguments);
}

function _preload() {
  _preload = _asyncToGenerator( /*#__PURE__*/regenerator.mark(function _callee() {
    var res, data;
    return regenerator.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return this.fetch("about.json");

          case 2:
            res = _context.sent;
            _context.next = 5;
            return res.json();

          case 5:
            data = _context.sent;

            if (!(res.status === 200)) {
              _context.next = 10;
              break;
            }

            return _context.abrupt("return", _objectSpread({}, data));

          case 10:
            this.error(res.status, data.message);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _preload.apply(this, arguments);
}

function instance($$self, $$props, $$invalidate) {
  var title = $$props.title;
  var body = $$props.body;
  var image = $$props.image;
  var writable_props = ["title", "body", "image"];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn("<About> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("About", $$slots, []);

  $$self.$set = function ($$props) {
    if ("title" in $$props) $$invalidate(0, title = $$props.title);
    if ("body" in $$props) $$invalidate(1, body = $$props.body);
    if ("image" in $$props) $$invalidate(2, image = $$props.image);
  };

  $$self.$capture_state = function () {
    return {
      preload: preload,
      Icon: Icon,
      Button: Button,
      PageCTA: PageCTA,
      Container: Container,
      title: title,
      body: body,
      image: image
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("title" in $$props) $$invalidate(0, title = $$props.title);
    if ("body" in $$props) $$invalidate(1, body = $$props.body);
    if ("image" in $$props) $$invalidate(2, image = $$props.image);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  return [title, body, image];
}

var About = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(About, _SvelteComponentDev);

  var _super = _createSuper(About);

  function About(options) {
    var _this;

    _classCallCheck(this, About);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance, create_fragment, safe_not_equal, {
      title: 0,
      body: 1,
      image: 2
    });
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "About",
      options: options,
      id: create_fragment.name
    });
    var ctx = _this.$$.ctx;
    var props = options.props || {};

    if (
    /*title*/
    ctx[0] === undefined && !("title" in props)) {
      console.warn("<About> was created without expected prop 'title'");
    }

    if (
    /*body*/
    ctx[1] === undefined && !("body" in props)) {
      console.warn("<About> was created without expected prop 'body'");
    }

    if (
    /*image*/
    ctx[2] === undefined && !("image" in props)) {
      console.warn("<About> was created without expected prop 'image'");
    }

    return _this;
  }

  _createClass(About, [{
    key: "title",
    get: function get() {
      throw new Error("<About>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<About>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "body",
    get: function get() {
      throw new Error("<About>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<About>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "image",
    get: function get() {
      throw new Error("<About>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<About>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }]);

  return About;
}(SvelteComponentDev);

export default About;
export { preload };
