import { c as _inherits, d as _getPrototypeOf, e as _possibleConstructorReturn, f as _classCallCheck, i as init, s as safe_not_equal, g as _assertThisInitialized, h as dispatch_dev, j as _createClass, S as SvelteComponentDev, v as validate_slots, K as Icon, ac as empty, z as insert_dev, b as _slicedToArray, a8 as group_outros, T as transition_out, a9 as check_outros, R as transition_in, q as detach_dev, an as bubble, Y as create_slot, l as element, m as space, M as create_component, n as claim_element, p as children, t as claim_space, O as claim_component, u as attr_dev, x as add_location, B as toggle_class, A as append_dev, P as mount_component, D as listen_dev, a2 as update_slot, ak as prop_dev, U as destroy_component, a4 as validate_each_argument, aa as destroy_each, L as text, N as claim_text, Q as set_data_dev } from './client.dc955f77.js';

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
var file = "Users/johan/projects/wayout/mono/packages/button/Button.svelte"; // (21:0) {:else}

function create_else_block(ctx) {
  var button;
  var t0;
  var span;
  var t1;
  var icon_1;
  var current;
  var mounted;
  var dispose;
  var default_slot_template =
  /*$$slots*/
  ctx[7].default;
  var default_slot = create_slot(default_slot_template, ctx,
  /*$$scope*/
  ctx[6], null);
  icon_1 = new Icon({
    props: {
      id:
      /*icon*/
      ctx[5]
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      button = element("button");
      if (default_slot) default_slot.c();
      t0 = space();
      span = element("span");
      t1 = space();
      create_component(icon_1.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      button = claim_element(nodes, "BUTTON", {
        class: true,
        disabled: true
      });
      var button_nodes = children(button);
      if (default_slot) default_slot.l(button_nodes);
      t0 = claim_space(button_nodes);
      span = claim_element(button_nodes, "SPAN", {
        class: true
      });
      children(span).forEach(detach_dev);
      t1 = claim_space(button_nodes);
      claim_component(icon_1.$$.fragment, button_nodes);
      button_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(span, "class", "hover-mark svelte-1fp7ofw");
      add_location(span, file, 29, 2, 468);
      attr_dev(button, "class", "button svelte-1fp7ofw");
      button.disabled =
      /*disabled*/
      ctx[2];
      toggle_class(button, "inverted",
      /*inverted*/
      ctx[0]);
      toggle_class(button, "loading",
      /*loading*/
      ctx[1]);
      add_location(button, file, 21, 0, 338);
    },
    m: function mount(target, anchor) {
      insert_dev(target, button, anchor);

      if (default_slot) {
        default_slot.m(button, null);
      }

      append_dev(button, t0);
      append_dev(button, span);
      append_dev(button, t1);
      mount_component(icon_1, button, null);
      current = true;

      if (!mounted) {
        dispose = listen_dev(button, "click",
        /*click_handler*/
        ctx[8], false, false, false);
        mounted = true;
      }
    },
    p: function update(ctx, dirty) {
      if (default_slot) {
        if (default_slot.p && dirty &
        /*$$scope*/
        64) {
          update_slot(default_slot, default_slot_template, ctx,
          /*$$scope*/
          ctx[6], dirty, null, null);
        }
      }

      var icon_1_changes = {};
      if (dirty &
      /*icon*/
      32) icon_1_changes.id =
      /*icon*/
      ctx[5];
      icon_1.$set(icon_1_changes);

      if (!current || dirty &
      /*disabled*/
      4) {
        prop_dev(button, "disabled",
        /*disabled*/
        ctx[2]);
      }

      if (dirty &
      /*inverted*/
      1) {
        toggle_class(button, "inverted",
        /*inverted*/
        ctx[0]);
      }

      if (dirty &
      /*loading*/
      2) {
        toggle_class(button, "loading",
        /*loading*/
        ctx[1]);
      }
    },
    i: function intro(local) {
      if (current) return;
      transition_in(default_slot, local);
      transition_in(icon_1.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(default_slot, local);
      transition_out(icon_1.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(button);
      if (default_slot) default_slot.d(detaching);
      destroy_component(icon_1);
      mounted = false;
      dispose();
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_else_block.name,
    type: "else",
    source: "(21:0) {:else}",
    ctx: ctx
  });
  return block;
} // (12:0) {#if href}


function create_if_block(ctx) {
  var a;
  var t0;
  var span;
  var t1;
  var icon_1;
  var current;
  var default_slot_template =
  /*$$slots*/
  ctx[7].default;
  var default_slot = create_slot(default_slot_template, ctx,
  /*$$scope*/
  ctx[6], null);
  icon_1 = new Icon({
    props: {
      id:
      /*icon*/
      ctx[5]
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      a = element("a");
      if (default_slot) default_slot.c();
      t0 = space();
      span = element("span");
      t1 = space();
      create_component(icon_1.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      a = claim_element(nodes, "A", {
        href: true,
        target: true,
        class: true
      });
      var a_nodes = children(a);
      if (default_slot) default_slot.l(a_nodes);
      t0 = claim_space(a_nodes);
      span = claim_element(a_nodes, "SPAN", {
        class: true
      });
      children(span).forEach(detach_dev);
      t1 = claim_space(a_nodes);
      claim_component(icon_1.$$.fragment, a_nodes);
      a_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(span, "class", "hover-mark svelte-1fp7ofw");
      add_location(span, file, 17, 2, 271);
      attr_dev(a, "href",
      /*href*/
      ctx[4]);
      attr_dev(a, "target",
      /*target*/
      ctx[3]);
      attr_dev(a, "class", "button svelte-1fp7ofw");
      add_location(a, file, 12, 0, 204);
    },
    m: function mount(target, anchor) {
      insert_dev(target, a, anchor);

      if (default_slot) {
        default_slot.m(a, null);
      }

      append_dev(a, t0);
      append_dev(a, span);
      append_dev(a, t1);
      mount_component(icon_1, a, null);
      current = true;
    },
    p: function update(ctx, dirty) {
      if (default_slot) {
        if (default_slot.p && dirty &
        /*$$scope*/
        64) {
          update_slot(default_slot, default_slot_template, ctx,
          /*$$scope*/
          ctx[6], dirty, null, null);
        }
      }

      var icon_1_changes = {};
      if (dirty &
      /*icon*/
      32) icon_1_changes.id =
      /*icon*/
      ctx[5];
      icon_1.$set(icon_1_changes);

      if (!current || dirty &
      /*href*/
      16) {
        attr_dev(a, "href",
        /*href*/
        ctx[4]);
      }

      if (!current || dirty &
      /*target*/
      8) {
        attr_dev(a, "target",
        /*target*/
        ctx[3]);
      }
    },
    i: function intro(local) {
      if (current) return;
      transition_in(default_slot, local);
      transition_in(icon_1.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(default_slot, local);
      transition_out(icon_1.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(a);
      if (default_slot) default_slot.d(detaching);
      destroy_component(icon_1);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_if_block.name,
    type: "if",
    source: "(12:0) {#if href}",
    ctx: ctx
  });
  return block;
}

function create_fragment(ctx) {
  var current_block_type_index;
  var if_block;
  var if_block_anchor;
  var current;
  var if_block_creators = [create_if_block, create_else_block];
  var if_blocks = [];

  function select_block_type(ctx, dirty) {
    if (
    /*href*/
    ctx[4]) return 0;
    return 1;
  }

  current_block_type_index = select_block_type(ctx);
  if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
  var block = {
    c: function create() {
      if_block.c();
      if_block_anchor = empty();
    },
    l: function claim(nodes) {
      if_block.l(nodes);
      if_block_anchor = empty();
    },
    m: function mount(target, anchor) {
      if_blocks[current_block_type_index].m(target, anchor);
      insert_dev(target, if_block_anchor, anchor);
      current = true;
    },
    p: function update(ctx, _ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];

      var previous_block_index = current_block_type_index;
      current_block_type_index = select_block_type(ctx);

      if (current_block_type_index === previous_block_index) {
        if_blocks[current_block_type_index].p(ctx, dirty);
      } else {
        group_outros();
        transition_out(if_blocks[previous_block_index], 1, 1, function () {
          if_blocks[previous_block_index] = null;
        });
        check_outros();
        if_block = if_blocks[current_block_type_index];

        if (!if_block) {
          if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](ctx);
          if_block.c();
        }

        transition_in(if_block, 1);
        if_block.m(if_block_anchor.parentNode, if_block_anchor);
      }
    },
    i: function intro(local) {
      if (current) return;
      transition_in(if_block);
      current = true;
    },
    o: function outro(local) {
      transition_out(if_block);
      current = false;
    },
    d: function destroy(detaching) {
      if_blocks[current_block_type_index].d(detaching);
      if (detaching) detach_dev(if_block_anchor);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function instance($$self, $$props, $$invalidate) {
  var inverted = $$props.inverted;
  var loading = $$props.loading;
  var disabled = $$props.disabled;
  var target = $$props.target;
  var href = $$props.href;
  var _$$props$icon = $$props.icon,
      icon = _$$props$icon === void 0 ? "ARROW" : _$$props$icon;
  var writable_props = ["inverted", "loading", "disabled", "target", "href", "icon"];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn("<Button> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("Button", $$slots, ['default']);

  function click_handler(event) {
    bubble($$self, event);
  }

  $$self.$set = function ($$props) {
    if ("inverted" in $$props) $$invalidate(0, inverted = $$props.inverted);
    if ("loading" in $$props) $$invalidate(1, loading = $$props.loading);
    if ("disabled" in $$props) $$invalidate(2, disabled = $$props.disabled);
    if ("target" in $$props) $$invalidate(3, target = $$props.target);
    if ("href" in $$props) $$invalidate(4, href = $$props.href);
    if ("icon" in $$props) $$invalidate(5, icon = $$props.icon);
    if ("$$scope" in $$props) $$invalidate(6, $$scope = $$props.$$scope);
  };

  $$self.$capture_state = function () {
    return {
      Icon: Icon,
      inverted: inverted,
      loading: loading,
      disabled: disabled,
      target: target,
      href: href,
      icon: icon
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("inverted" in $$props) $$invalidate(0, inverted = $$props.inverted);
    if ("loading" in $$props) $$invalidate(1, loading = $$props.loading);
    if ("disabled" in $$props) $$invalidate(2, disabled = $$props.disabled);
    if ("target" in $$props) $$invalidate(3, target = $$props.target);
    if ("href" in $$props) $$invalidate(4, href = $$props.href);
    if ("icon" in $$props) $$invalidate(5, icon = $$props.icon);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  return [inverted, loading, disabled, target, href, icon, $$scope, $$slots, click_handler];
}

var Button = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(Button, _SvelteComponentDev);

  var _super = _createSuper(Button);

  function Button(options) {
    var _this;

    _classCallCheck(this, Button);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance, create_fragment, safe_not_equal, {
      inverted: 0,
      loading: 1,
      disabled: 2,
      target: 3,
      href: 4,
      icon: 5
    });
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "Button",
      options: options,
      id: create_fragment.name
    });
    var ctx = _this.$$.ctx;
    var props = options.props || {};

    if (
    /*inverted*/
    ctx[0] === undefined && !("inverted" in props)) {
      console.warn("<Button> was created without expected prop 'inverted'");
    }

    if (
    /*loading*/
    ctx[1] === undefined && !("loading" in props)) {
      console.warn("<Button> was created without expected prop 'loading'");
    }

    if (
    /*disabled*/
    ctx[2] === undefined && !("disabled" in props)) {
      console.warn("<Button> was created without expected prop 'disabled'");
    }

    if (
    /*target*/
    ctx[3] === undefined && !("target" in props)) {
      console.warn("<Button> was created without expected prop 'target'");
    }

    if (
    /*href*/
    ctx[4] === undefined && !("href" in props)) {
      console.warn("<Button> was created without expected prop 'href'");
    }

    return _this;
  }

  _createClass(Button, [{
    key: "inverted",
    get: function get() {
      throw new Error("<Button>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Button>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "loading",
    get: function get() {
      throw new Error("<Button>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Button>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "disabled",
    get: function get() {
      throw new Error("<Button>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Button>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "target",
    get: function get() {
      throw new Error("<Button>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Button>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "href",
    get: function get() {
      throw new Error("<Button>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Button>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }, {
    key: "icon",
    get: function get() {
      throw new Error("<Button>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Button>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }]);

  return Button;
}(SvelteComponentDev);

function _createSuper$1(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct$1(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct$1() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
var file$1 = "src/components/PageCTA.svelte";

function get_each_context(ctx, list, i) {
  var child_ctx = ctx.slice();
  child_ctx[1] = list[i].link;
  child_ctx[2] = list[i].text;
  return child_ctx;
} // (11:8) <Button href={link}>


function create_default_slot_1(ctx) {
  var t_value =
  /*text*/
  ctx[2] + "";
  var t;
  var block = {
    c: function create() {
      t = text(t_value);
    },
    l: function claim(nodes) {
      t = claim_text(nodes, t_value);
    },
    m: function mount(target, anchor) {
      insert_dev(target, t, anchor);
    },
    p: function update(ctx, dirty) {
      if (dirty &
      /*links*/
      1 && t_value !== (t_value =
      /*text*/
      ctx[2] + "")) set_data_dev(t, t_value);
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(t);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot_1.name,
    type: "slot",
    source: "(11:8) <Button href={link}>",
    ctx: ctx
  });
  return block;
} // (9:4) {#each links as {link, text}}


function create_each_block(ctx) {
  var li;
  var button;
  var current;
  button = new Button({
    props: {
      href:
      /*link*/
      ctx[1],
      $$slots: {
        default: [create_default_slot_1]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      li = element("li");
      create_component(button.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      li = claim_element(nodes, "LI", {
        class: true
      });
      var li_nodes = children(li);
      claim_component(button.$$.fragment, li_nodes);
      li_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(li, "class", "svelte-1wjl3ak");
      add_location(li, file$1, 9, 6, 133);
    },
    m: function mount(target, anchor) {
      insert_dev(target, li, anchor);
      mount_component(button, li, null);
      current = true;
    },
    p: function update(ctx, dirty) {
      var button_changes = {};
      if (dirty &
      /*links*/
      1) button_changes.href =
      /*link*/
      ctx[1];

      if (dirty &
      /*$$scope, links*/
      33) {
        button_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      button.$set(button_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(button.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(button.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(li);
      destroy_component(button);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_each_block.name,
    type: "each",
    source: "(9:4) {#each links as {link, text}}",
    ctx: ctx
  });
  return block;
} // (16:8) <Button href="http://wayoutwater.com" target="_blank" icon="ARROW_EXTERNAL">


function create_default_slot(ctx) {
  var t;
  var block = {
    c: function create() {
      t = text("Wayout Water");
    },
    l: function claim(nodes) {
      t = claim_text(nodes, "Wayout Water");
    },
    m: function mount(target, anchor) {
      insert_dev(target, t, anchor);
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(t);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot.name,
    type: "slot",
    source: "(16:8) <Button href=\\\"http://wayoutwater.com\\\" target=\\\"_blank\\\" icon=\\\"ARROW_EXTERNAL\\\">",
    ctx: ctx
  });
  return block;
}

function create_fragment$1(ctx) {
  var nav;
  var ul;
  var t;
  var li;
  var button;
  var current;
  var each_value =
  /*links*/
  ctx[0];
  validate_each_argument(each_value);
  var each_blocks = [];

  for (var i = 0; i < each_value.length; i += 1) {
    each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
  }

  var out = function out(i) {
    return transition_out(each_blocks[i], 1, 1, function () {
      each_blocks[i] = null;
    });
  };

  button = new Button({
    props: {
      href: "http://wayoutwater.com",
      target: "_blank",
      icon: "ARROW_EXTERNAL",
      $$slots: {
        default: [create_default_slot]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      nav = element("nav");
      ul = element("ul");

      for (var _i = 0; _i < each_blocks.length; _i += 1) {
        each_blocks[_i].c();
      }

      t = space();
      li = element("li");
      create_component(button.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      nav = claim_element(nodes, "NAV", {
        class: true
      });
      var nav_nodes = children(nav);
      ul = claim_element(nav_nodes, "UL", {
        class: true
      });
      var ul_nodes = children(ul);

      for (var _i2 = 0; _i2 < each_blocks.length; _i2 += 1) {
        each_blocks[_i2].l(ul_nodes);
      }

      t = claim_space(ul_nodes);
      li = claim_element(ul_nodes, "LI", {
        class: true
      });
      var li_nodes = children(li);
      claim_component(button.$$.fragment, li_nodes);
      li_nodes.forEach(detach_dev);
      ul_nodes.forEach(detach_dev);
      nav_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(li, "class", "svelte-1wjl3ak");
      add_location(li, file$1, 14, 4, 217);
      attr_dev(ul, "class", "svelte-1wjl3ak");
      add_location(ul, file$1, 7, 2, 88);
      attr_dev(nav, "class", "svelte-1wjl3ak");
      add_location(nav, file$1, 6, 0, 80);
    },
    m: function mount(target, anchor) {
      insert_dev(target, nav, anchor);
      append_dev(nav, ul);

      for (var _i3 = 0; _i3 < each_blocks.length; _i3 += 1) {
        each_blocks[_i3].m(ul, null);
      }

      append_dev(ul, t);
      append_dev(ul, li);
      mount_component(button, li, null);
      current = true;
    },
    p: function update(ctx, _ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];

      if (dirty &
      /*links*/
      1) {
        each_value =
        /*links*/
        ctx[0];
        validate_each_argument(each_value);

        var _i4;

        for (_i4 = 0; _i4 < each_value.length; _i4 += 1) {
          var child_ctx = get_each_context(ctx, each_value, _i4);

          if (each_blocks[_i4]) {
            each_blocks[_i4].p(child_ctx, dirty);

            transition_in(each_blocks[_i4], 1);
          } else {
            each_blocks[_i4] = create_each_block(child_ctx);

            each_blocks[_i4].c();

            transition_in(each_blocks[_i4], 1);

            each_blocks[_i4].m(ul, t);
          }
        }

        group_outros();

        for (_i4 = each_value.length; _i4 < each_blocks.length; _i4 += 1) {
          out(_i4);
        }

        check_outros();
      }

      var button_changes = {};

      if (dirty &
      /*$$scope*/
      32) {
        button_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      button.$set(button_changes);
    },
    i: function intro(local) {
      if (current) return;

      for (var _i5 = 0; _i5 < each_value.length; _i5 += 1) {
        transition_in(each_blocks[_i5]);
      }

      transition_in(button.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      each_blocks = each_blocks.filter(Boolean);

      for (var _i6 = 0; _i6 < each_blocks.length; _i6 += 1) {
        transition_out(each_blocks[_i6]);
      }

      transition_out(button.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(nav);
      destroy_each(each_blocks, detaching);
      destroy_component(button);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment$1.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function instance$1($$self, $$props, $$invalidate) {
  var links = $$props.links;
  var writable_props = ["links"];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn("<PageCTA> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("PageCTA", $$slots, []);

  $$self.$set = function ($$props) {
    if ("links" in $$props) $$invalidate(0, links = $$props.links);
  };

  $$self.$capture_state = function () {
    return {
      Button: Button,
      links: links
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("links" in $$props) $$invalidate(0, links = $$props.links);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  return [links];
}

var PageCTA = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(PageCTA, _SvelteComponentDev);

  var _super = _createSuper$1(PageCTA);

  function PageCTA(options) {
    var _this;

    _classCallCheck(this, PageCTA);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance$1, create_fragment$1, safe_not_equal, {
      links: 0
    });
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "PageCTA",
      options: options,
      id: create_fragment$1.name
    });
    var ctx = _this.$$.ctx;
    var props = options.props || {};

    if (
    /*links*/
    ctx[0] === undefined && !("links" in props)) {
      console.warn("<PageCTA> was created without expected prop 'links'");
    }

    return _this;
  }

  _createClass(PageCTA, [{
    key: "links",
    get: function get() {
      throw new Error("<PageCTA>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<PageCTA>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }]);

  return PageCTA;
}(SvelteComponentDev);

export { Button as B, PageCTA as P };
