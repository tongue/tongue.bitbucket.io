import { a as _asyncToGenerator, r as regenerator, c as _inherits, d as _getPrototypeOf, e as _possibleConstructorReturn, f as _classCallCheck, i as init, s as safe_not_equal, g as _assertThisInitialized, h as dispatch_dev, j as _createClass, S as SvelteComponentDev, v as validate_slots, m as space, l as element, M as create_component, ad as query_selector_all, q as detach_dev, t as claim_space, n as claim_element, p as children, O as claim_component, x as add_location, z as insert_dev, P as mount_component, b as _slicedToArray, R as transition_in, T as transition_out, U as destroy_component, u as attr_dev, y as set_style } from './client.dc955f77.js';
import { C as Container } from './Container.19136de0.js';

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }
var file = "src/routes/consent.svelte"; // (21:2) <Container>

function create_default_slot(ctx) {
  var div;
  var block = {
    c: function create() {
      div = element("div");
      this.h();
    },
    l: function claim(nodes) {
      div = claim_element(nodes, "DIV", {
        class: true,
        style: true
      });
      var div_nodes = children(div);
      div_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      attr_dev(div, "class", "discrete svelte-uuu1uv");
      set_style(div, "margin-bottom", "100px");
      add_location(div, file, 21, 4, 398);
    },
    m: function mount(target, anchor) {
      insert_dev(target, div, anchor);
      div.innerHTML =
      /*content*/
      ctx[0];
    },
    p: function update(ctx, dirty) {
      if (dirty &
      /*content*/
      1) div.innerHTML =
      /*content*/
      ctx[0];
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(div);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_default_slot.name,
    type: "slot",
    source: "(21:2) <Container>",
    ctx: ctx
  });
  return block;
}

function create_fragment(ctx) {
  var t;
  var main;
  var container;
  var current;
  container = new Container({
    props: {
      $$slots: {
        default: [create_default_slot]
      },
      $$scope: {
        ctx: ctx
      }
    },
    $$inline: true
  });
  var block = {
    c: function create() {
      t = space();
      main = element("main");
      create_component(container.$$.fragment);
      this.h();
    },
    l: function claim(nodes) {
      var head_nodes = query_selector_all("[data-svelte=\"svelte-o5yu05\"]", document.head);
      head_nodes.forEach(detach_dev);
      t = claim_space(nodes);
      main = claim_element(nodes, "MAIN", {});
      var main_nodes = children(main);
      claim_component(container.$$.fragment, main_nodes);
      main_nodes.forEach(detach_dev);
      this.h();
    },
    h: function hydrate() {
      document.title = "Wayout - Privacy Policy";
      add_location(main, file, 19, 0, 373);
    },
    m: function mount(target, anchor) {
      insert_dev(target, t, anchor);
      insert_dev(target, main, anchor);
      mount_component(container, main, null);
      current = true;
    },
    p: function update(ctx, _ref) {
      var _ref2 = _slicedToArray(_ref, 1),
          dirty = _ref2[0];

      var container_changes = {};

      if (dirty &
      /*$$scope, content*/
      3) {
        container_changes.$$scope = {
          dirty: dirty,
          ctx: ctx
        };
      }

      container.$set(container_changes);
    },
    i: function intro(local) {
      if (current) return;
      transition_in(container.$$.fragment, local);
      current = true;
    },
    o: function outro(local) {
      transition_out(container.$$.fragment, local);
      current = false;
    },
    d: function destroy(detaching) {
      if (detaching) detach_dev(t);
      if (detaching) detach_dev(main);
      destroy_component(container);
    }
  };
  dispatch_dev("SvelteRegisterBlock", {
    block: block,
    id: create_fragment.name,
    type: "component",
    source: "",
    ctx: ctx
  });
  return block;
}

function preload() {
  return _preload.apply(this, arguments);
}

function _preload() {
  _preload = _asyncToGenerator( /*#__PURE__*/regenerator.mark(function _callee() {
    var res, page;
    return regenerator.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return this.fetch("consent.json");

          case 2:
            res = _context.sent;
            _context.next = 5;
            return res.json();

          case 5:
            page = _context.sent;
            return _context.abrupt("return", {
              content: page.content
            });

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));
  return _preload.apply(this, arguments);
}

function instance($$self, $$props, $$invalidate) {
  var content = $$props.content;
  var writable_props = ["content"];
  Object.keys($$props).forEach(function (key) {
    if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn("<Consent> was created with unknown prop '".concat(key, "'"));
  });
  var _$$props$$$slots = $$props.$$slots,
      $$slots = _$$props$$$slots === void 0 ? {} : _$$props$$$slots,
      $$scope = $$props.$$scope;
  validate_slots("Consent", $$slots, []);

  $$self.$set = function ($$props) {
    if ("content" in $$props) $$invalidate(0, content = $$props.content);
  };

  $$self.$capture_state = function () {
    return {
      Container: Container,
      preload: preload,
      content: content
    };
  };

  $$self.$inject_state = function ($$props) {
    if ("content" in $$props) $$invalidate(0, content = $$props.content);
  };

  if ($$props && "$$inject" in $$props) {
    $$self.$inject_state($$props.$$inject);
  }

  return [content];
}

var Consent = /*#__PURE__*/function (_SvelteComponentDev) {
  _inherits(Consent, _SvelteComponentDev);

  var _super = _createSuper(Consent);

  function Consent(options) {
    var _this;

    _classCallCheck(this, Consent);

    _this = _super.call(this, options);
    init(_assertThisInitialized(_this), options, instance, create_fragment, safe_not_equal, {
      content: 0
    });
    dispatch_dev("SvelteRegisterComponent", {
      component: _assertThisInitialized(_this),
      tagName: "Consent",
      options: options,
      id: create_fragment.name
    });
    var ctx = _this.$$.ctx;
    var props = options.props || {};

    if (
    /*content*/
    ctx[0] === undefined && !("content" in props)) {
      console.warn("<Consent> was created without expected prop 'content'");
    }

    return _this;
  }

  _createClass(Consent, [{
    key: "content",
    get: function get() {
      throw new Error("<Consent>: Props cannot be read directly from the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    },
    set: function set(value) {
      throw new Error("<Consent>: Props cannot be set directly on the component instance unless compiling with 'accessors: true' or '<svelte:options accessors/>'");
    }
  }]);

  return Consent;
}(SvelteComponentDev);

export default Consent;
export { preload };
